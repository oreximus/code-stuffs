#include <iostream>
using namespace std;

// Program to calculate
// the gross salary
// where one allowance is
// 40% of the basic salary and
// other allowance is 20%
// of the basic salary
// and basic salary is a input by
// user

int main(){

  float s,a1,a2,g_s;

  cout << "Enter the Basic Salary: ";
  cin >> s;

  a1 = s*0.20;
  a2 = s*0.40;

  g_s = s+a1+a2;

  cout << "\nThe Gross Salary is: " << g_s;
  
  return 0;
}
