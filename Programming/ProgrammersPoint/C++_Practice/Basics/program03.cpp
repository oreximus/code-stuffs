#include <iostream>
using namespace std;

// Program to do the
// sum of two numbers
// entered by the user

int main(){
  int a,b,c;

  cout << "Enter two integer values: ";
  cin >> a >> b;

  c = a + b;
  
  cout << "The sum of " << a << " and " << b << " is: " << c;
  return 0;

}
