#include <iostream>
#include <iomanip>
#define size 5
using namespace std;

// Program to calculate
// the average of 5 subject
// marks

int main(){

  int a[size], i;
  float sum=0, avg;

  cout << "Enter the Marks for size Subjects:\n";

  for(i=0;i<size;i++){
    cin >> a[i];
    sum = sum + a[i];
  }

  avg = sum/size;

  cout << "Sum of " << size << " Subjects is: " << sum;

  cout << fixed;

  cout << setprecision(2) << "\nAverage of all " << size << " is: " << avg;

  return 0;
}
