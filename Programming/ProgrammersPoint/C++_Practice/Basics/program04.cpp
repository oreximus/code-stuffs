#include <iostream>
using namespace std;

// Program to calculate the
// Simple interest of any
// Principle amount

int main(){

  int r,t;
  float p,si;

  cout << "Enter the principle amount: ";
  cin >> p;
  cout << "Enter the Rate Percentage: ";
  cin >> r;
  cout << "Enter the Time Period: ";
  cin >> t;

  si = (p*r*t)/100;

  cout << "Simple Interest for Principle: " << p << ", Rate: " << r << ", Time: " << t << " is: " << si;

  return 0;

}
