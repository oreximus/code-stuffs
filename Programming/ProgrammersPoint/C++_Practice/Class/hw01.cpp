#include <iostream>
using namespace std;

  // checking if we can declare a friend
  // function in multiple classes
  //
  // And the program proves that YES we can
  // by creating objects for each class

class Sum {

  private:
    int a,b,c;

    void get(){
      cout << "Enter two values: ";
      cin >> a >> b;
    }

    void sum(){
      c = a+b;
    }

    void show(){
      cout << "\nSum of " << a << " and " << b << " is: " << c;
    }

    friend void oper();

};

class Sub {

  private:
    int a,b,c;

    void get(){
      cout << "Enter two values: ";
      cin >> a >> b;
    }

    void sub(){
      c = a-b;
    }

    void show(){
      cout << "\nDifference of " << a << " and " << b << " is: " << c;
    }

    friend void oper();

};

class Prod {

  private:
    int a,b,c;

    void get(){
      cout << "Enter two values: ";
      cin >> a >> b;
    }

    void prod(){
      c = a*b;
    }

    void show(){
      cout << "\nProduct of " << a << " and " << b << " is: " << c;
    }

    friend void oper();

};

void oper(){

  Sum s1;
  Sub s2;
  Prod s3;
  s1.get();
  s2.get();
  s3.get();
  s1.sum();
  s2.sub();
  s3.prod();
  s1.show();
  s2.show();
  s3.show();
}
int main(){

  oper();

  return 0;

}
