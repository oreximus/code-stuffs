#include <iostream>
using namespace std;

// Program to show
// Hybrid Inheritance
// example


class Get{

  public:
    int a,b,c;

    void get(){
      cout << "\nEnter two values:\n";
      cin >> a >> b;
    }

    void show(string msg){
      cout << msg << " of " << a << " and " << b << " is: " << c;
    }
};

class Sum: public Get{

  public:
    void sum(){
      c = a+b;
    }
};

class Product: public Get{

  public:
    void pro(){
      c = a*b;
    }
};

int main(){

  Sum s1;
  Product p1;


  s1.get();
  s1.sum();
  s1.show("Sum");

  p1.get();
  p1.pro();
  p1.show("Product");

  return 0;
}
