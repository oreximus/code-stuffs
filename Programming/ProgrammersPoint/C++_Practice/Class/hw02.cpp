#include <iostream>
using namespace std;

// Checking if we can declare
// a main function in any class
//
// Yes, we can declare the main
// function as friend function in
// a class, only the need is to
// make defined the function like
// it returns the integer type value.

class Sum{

  private:
    int a,b,c;

    void get(){
      cout << "Enter two values: ";
      cin >> a >> b;
    }

    void sum(){
      c = a+b;
    }

    void show(){
      cout << "\nSum of " << a << " and " << b << " is " << c;
    }

    friend int main();
};

int main(){
  Sum s1;
  s1.get();
  s1.sum();
  s1.show();

  return 0;
}
