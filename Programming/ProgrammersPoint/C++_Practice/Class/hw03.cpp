#include <iostream>
#include <iomanip>
#define size 5
using namespace std;

// Program to get
// Students marks 
// for 5 Subjects
// and Based on 
// those subjects
// provides average
// total and percentage
// of student with 
// unique roll no

class Student{

  private:
    int i,marks[size],total=0,roll_no=100;
    float avg,per;

  public:

    Student(){
      cout << "\nStudent Marks Calculator!";
    }
    void get(){
      cout << "\nEnter the marks for "<< size <<" subjects:\n";
      for(i=0;i<size;i++){
        cin >> marks[i];
        total += marks[i];
      }
    }
    void all(){
      roll_no = roll_no+1;
      cout << "\nRoll No:" << roll_no;
      cout << "\ntotal: " << total <<"/" << size*100;
    }
    void average(){
      avg = total/(float)size;
      cout << fixed;
      cout << setprecision(2) << "\nAverage of all "<< size <<" is: " << avg;
      total = 0;
    }
};

int main(){

  int resp;

  Student s1;

  do{
    s1.get();
    s1.all();
    s1.average();
    cout << "\nPress 1 to Continue or Enter any other Key to exit: ";
    cin >> resp;
  }
  while(resp == 1);

  return 0;

}
