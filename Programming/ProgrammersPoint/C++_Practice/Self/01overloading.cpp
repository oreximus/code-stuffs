#include <iostream>
using namespace std;

class Cool{

  int x,y,z;
  public:
  void getdata(int a, int b, int c){
    x=a;
    y=b;
    z=c;
  }
  void display()
  {
    cout <<"\nx= "<<x<<"\ny= "<<y<<"\nz= "<<z<<"\n";
  }
  void operator-(){
    x=-x;
    y=-y;
    z=-z;
  }
};

int main(){

  Cool c1;

  cout << "\nBefore Doing Operator Overloading\n";

  c1.getdata(3,5,7);
  c1.display();

  -c1;         // activates operator-() function

  cout << "\nAfter Doing Operator Overloading\n";

  c1.display();

  return 0;

}
