#include <stdio.h>
#include <math.h>

int main(){

  // program to print
  // the sum of series

  int i,x,n,j=1;
  double s=0;
  printf("\nEnter the value of x: ");
  scanf("%d", &x);
  printf("\nEnter the value of n: ");
  scanf("%d", &n);

  for (i=1;i<=n;i++) {


    if (j%2 != 0){
    s = s + pow(x,i);
    }
    else{
    s = s - pow(x,i);
    }

    j++;

  }

  printf("\nResult is %.0lf", s);
  return 0;
}
