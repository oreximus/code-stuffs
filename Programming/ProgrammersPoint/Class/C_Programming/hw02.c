#include <stdio.h>
#define size 8

int main(){

  int a[size],i,pn[size],c=1,j;

  printf("\nEnter %d values:\n", size);

  for(i=0;i<size;i++){
    scanf("%d",&a[i]);
  }

  for(i=0;i<size;i++){
    for(j=2;j<=a[i];j++){
      if(a[i]%j != 0){
        pn[i] = a[i];
        c++;
      }
    }
  }

  printf("\nAll %d Prime are: ", c);

  for(i=0;i<c;i++){
    printf("%d\n",pn[i]);
  }

  return 0;

}
