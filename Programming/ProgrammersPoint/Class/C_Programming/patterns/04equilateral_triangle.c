#include <stdio.h>

int main(){

  // Program to print
  // pattern using *
  // to form equilateral
  // triangle
  
  int i,j;

  for(i=1;i<10;i++){
    for(j=4;j>i;j--){
      printf(" ");
    }
    for(j=1;j<=i;j++){
      printf(" *");
    }
    printf("\n");
  }

  return 0;
}
