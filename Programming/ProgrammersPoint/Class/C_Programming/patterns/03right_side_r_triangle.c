#include <stdio.h>

int main(){

  // Program to print a
  // Right-angle Triangle pattern using
  // * through loop
  
  int i,j;

  for(i=1;i<=10;i++){ // First Loop for No of Rows
    for(j=10;j>i;j--){
      printf(" ");
    }
    for(j=1;j<=i;j++){
      printf("*");
    }
    printf("\n");
  }

  return 0;
}
