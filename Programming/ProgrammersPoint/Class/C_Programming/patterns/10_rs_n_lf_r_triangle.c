#include <stdio.h>

int main(){

  // Program to print a left side
  // right side Right-angle Triangle
  // pattern using * through loop
  
  int i,j,n=18;

  for(i=1;i<=10;i++){ // First Loop for No of Rows
    for(j=1;j<=i;j++){
      printf("*");
    }
    for(j=1;j<=n;j++){
      printf(" ");
    }
    for(j=1;j<=i;j++){
      printf("*");
    }
    printf("\n");
    n=n-2;
  }

  return 0;
}
