#include <stdio.h>

int main(){

  // Program to print
  // pattern using *
  // to form an xmas tree
  
  int i,j;

  for(i=1;i<10;i++){
    for(j=10;j>i;j--){
      printf(" ");
    }
    for(j=1;j<=i;j++){
      printf(" *");
    }
    printf("\n");
  }

  for(i=1;i<10;i++){
    for(j=10;j>i;j--){
      printf(" ");
    }
    for(j=1;j<=i;j++){
      printf(" *");
    }
    printf("\n");
  }

  for(i=1;i<10;i++){
    for(j=10;j>i;j--){
      printf(" ");
    }
    for(j=1;j<=i;j++){
      printf(" *");
    }
    printf("\n");
  }

  return 0;
}
