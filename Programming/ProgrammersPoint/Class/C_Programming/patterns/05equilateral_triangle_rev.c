#include <stdio.h>

int main(){

  // Program print pattern
  // using * to form
  // equilateral triangle
  // in reverse manner

  int i,j;

  for(i=1;i<10;i++){
    for(j=1;j<=i;j++){
      printf(" ");
    }
    for(j=10;j>i;j--){
      printf("* ");
    }
    printf("\n");
  }

  return 0;
}
