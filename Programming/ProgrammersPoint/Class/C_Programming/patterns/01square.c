#include <stdio.h>

int main(){

  // Program to print a
  // Square pattern using
  // * through loop
  
  int i,j;

  for(i=1;i<=5;i++){ // First Loop for No of Rows
    for(j=1;j<=5;j++){ // Second Loop for No of Columns
      printf("* ");
    }
    printf("\n");
  }

  return 0;
}
