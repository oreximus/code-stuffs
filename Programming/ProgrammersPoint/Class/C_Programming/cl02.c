#include <stdio.h>

int main(){

// Program to print
// this pattern:
//     1
//    212
//   32123
//  4321234
// 543212345

  int i,j,k;

  for(i=1;i<=5;i++){
    k = i;
    for(j=4;j>=i;j--){
      printf(" ");
    }
    for(j=1;j<=i;j++){
      printf("%d",k);
      k -= 1;
    }
    for(j=1;j<i;j++){
      printf("%d",j+1);
    }
    printf("\n");
  }

  return 0;
}
