#include <stdio.h>
#include <math.h>

int main(){

  // program to print
  // the power value of 
  // any input number in 
  int i,x,n;
  double s=0;
  printf("\nEnter the value of x: ");
  scanf("%d", &x);
  printf("\nEnter the value of n: ");
  scanf("%d", &n);

  for (i=0;i<=n;i++) {

    s = s + pow(x,i);
  }

  printf("\nResult is %.0lf", s);
  return 0;
}
