#include <stdio.h>
#define size 7

int main(){


  int i, a[size], s1 = 0;
  float avg;

  printf("\n Enter %d values: ", size);
  for(i=0;i<size;i++){
    scanf("%d", &a[i]);
  }

  printf("\nAll Values are: ");

  for(i=0;i<size;i++){
    printf("\n%d", a[i]);
    s1 = s1+a[i];
  }

  avg = (float)s1/size;

  printf("\nSum of all values is: %d", s1);
  return 0;

}
