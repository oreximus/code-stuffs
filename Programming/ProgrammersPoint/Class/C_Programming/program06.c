#include <stdio.h>

int main(){

  int i,j;
  
  for(i=1;i<=11;i++){ // No of rows
    for(j=1;j<=11;j++){ // No of columns
      if(i==1||i==11||j==1||j==i||j==11||j==12-i)
      printf("* ");
      else
      printf("  ");
  }
    printf("\n");
  }
  return 0;
}
