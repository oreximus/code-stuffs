#include <stdio.h>

// Priting this pattern:
//     1
//    234
//   56789
//  1234567
// 891234567

int main(){

  int i,j,k=1;

  for(i=1;i<=5;i++){

    for(j=4;j>=i;j--){
      printf(" ");
    }
    for(j=1;j<i*2;j++){
      printf("%d",k);
      if(k<9){
        k+=1;
      }
      else{
        k=1;
      }
    }
    printf("\n");
  }
}
