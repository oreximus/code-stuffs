#include <stdio.h>
#define size 10

int main(){

  // Program to find the maximum
  // value and it's position in 
  // a given array
  
  int i,max,mxp=0,a[size];

  printf("\nEnter %d values:\n", size);
  
  for(i=0;i<size;i++){
    scanf("%d", &a[i]);
  }

  max = a[i];

  for (i=0;i<size;i++) {

    if(max<a[i]){
      max = a[i];
      mxp = i;
    }
  }

  printf("\nThe maximum value in above array is: %d and it's postions is: %d", max,mxp+1);

  return 0;
}
