#include <stdio.h>

int main(){

  int i,j;
  
  for(i=1;i<=5;i++){ // No of rows
    for(j=4;j>=i;j--){ // No of columns
      printf(" ");
    }
    for(j=1;j<=i;j++){ // No of columns
      printf("*");
    }
    printf("\n");
  }
  return 0;
}
