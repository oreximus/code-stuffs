#include <stdio.h>

int main(){

  // program to print
  // the power value of 
  // any input number
  int i,x,n;
  double p=1;
  printf("\nEnter the value of x: ");
  scanf("%d", &x);
  printf("\nEnter the value of n: ");
  scanf("%d", &n);

  for (i=1;i<=n;i++) {

    p = p*x;
  }

  printf("\n%d to the power of %d is %.0lf", x,n,p);
  return 0;
}
