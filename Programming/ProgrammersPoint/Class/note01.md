## OOP

- Class
- Object
- Constructor
- Inheritance
- Polymorphism
- Encapsulation
- Data Abstraction

## Class:

Class is a collection of data members and member functions where data member are the variables which can store data and functions which can perform operations on those data members, class is also called user defined data type just like a structures in c.

## Object:

object is an instance of a class which contains or blueprint of a class or basic runtime entity, which contains all the properties and their behaviour of a class. And by the help of that we can access all the properties of a class outside the class.

## Access Modifiers
a way to access to the properties of class through objects!

- public
- protected
- private

## Scope Resolution Operator

- we use this to define the functions associated to some class.

## Friend Function

- It help us to use the features of a private,public,protected class.

## Encapsulation

It is a process by which we can wrap hold data and process in a single unit.

## Data Abstraction

we can show only essential features and hide the remaining

## Questions

1. can we declare the same friend function in multiple classes?

- Yes, we can define the same friend function in multiple classes and access the properties of the classes by creating objects in the the definition of the friend function.

```
#include <iostream>
using namespace std;

  // checking if we can declare a friend
  // function in multiple classes
  //
  // And the program proves that YES we can
  // by creating objects for each class

class Sum {

  private:
    int a,b,c;

    void get(){
      cout << "Enter two values: ";
      cin >> a >> b;
    }

    void sum(){
      c = a+b;
    }

    void show(){
      cout << "\nSum of " << a << " and " << b << " is: " << c;
    }

    friend void oper();

};

class Sub {

  private:
    int a,b,c;

    void get(){
      cout << "Enter two values: ";
      cin >> a >> b;
    }

    void sub(){
      c = a-b;
    }

    void show(){
      cout << "\nDifference of " << a << " and " << b << " is: " << c;
    }

    friend void oper();

};

class Prod {

  private:
    int a,b,c;

    void get(){
      cout << "Enter two values: ";
      cin >> a >> b;
    }

    void prod(){
      c = a*b;
    }

    void show(){
      cout << "\nProduct of " << a << " and " << b << " is: " << c;
    }

    friend void oper();

};

void oper(){

  Sum s1;
  Sub s2;
  Prod s3;
  s1.get();
  s2.get();
  s3.get();
  s1.sum();
  s2.sub();
  s3.prod();
  s1.show();
  s2.show();
  s3.show();
}
int main(){

  oper();

  return 0;

}
```

2. can we declare main function as a friend function in any class?

- Yes, we can declare the main function as the friend function in the class, only the need is to make it to return an integer value; for that we can specify this like: `friend int main();`.

```
#include <iostream>
using namespace std;

// Checking if we can declare
// a main function in any class

class Sum{

  private:
    int a,b,c;

    void get(){
      cout << "Enter two values: ";
      cin >> a >> b;
    }

    void sum(){
      c = a+b;
    }

    void show(){
      cout << "\nSum of " << a << " and " << b << " is " << c;
    }

    friend int main();
};

int main(){
  Sum s1;
  s1.get();
  s1.sum();
  s1.show();

  return 0;
}
```
