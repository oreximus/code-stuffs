#include <iostream>
using namespace std;

// Program to demonstrate
// the User Defined,
// Parameterized, and 
// Copy type of Constructors

class Sum{

  public:
    int a,b,c;
    
    Sum(){
      cout << "Default. ";
    }

    void get(){
      cout << "Enter two values: ";
      cin >> a >> b;
    }

    void sum(){
      c = a+b;
    }

    void show(){
      cout << "\n Sum of " << a << " and " << b << " is: " << c;
    }

    Sum(int x, int y){
      cout << "\nParameterized. ";
      a = x;
      b = y;
    }
};

int main(){

  Sum s1;
  s1.get();
  s1.sum();
  s1.show();

  Sum s2(34,28);
  s2.sum();
  s2.show();

  Sum s3 = s2;
  s3.show();

  return 0;
}
