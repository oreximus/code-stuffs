#include <iostream>
using namespace std;

// Program to Show the
// Multilevel inheritance
// in classes

class Person{
  public:
    string name, city;
    int age;
    void getP(string n1, string c1, int a1){

      name = n1;
      city = c1;
      age = a1;

    }

    void showP()
    {
      cout << "\n Name \t: " << name;
      cout << "\n City \t: " << city;
      cout << "\n Age \t: " << age;
    }
};

class Employee : public Person{

  public:
    int id;
    float sal;
    
    void getE(string n1, string c1, int a1, int i1, float s1)
    {
      getP(n1,c1,a1);
      id = i1;
      sal = s1;
    }
    void showE(){
      cout << "\nID \t: " << id;
      showP();
      cout << "\nSalary\t: " << sal;
    }
};

class Faculty: public Employee
{
  public:
    string sub;
    void getF(string n1, string c1, int a1, int i1, float s1, string s2){
      getE(n1,c1,a1,i1,s1);
      sub = s2;
    }
    void showF(){
      showE();
      cout << "\nSubject\t: "<< sub;
    }
};


int main(){

  // Program to demonstrate
  // multilevel inheritance
  
  class Person p1;
  class Employee e1;
  class Faculty f1;

  cout << "\nPerson Details:\n";

  p1.getP("Cool","CoolCity", 24);
  p1.showP();

  cout << "\n\nEmployee Details:\n";

  e1.getE("Some", "SomeCity", 32, 10023, 50000);
  e1.showE();

  cout << "\n\nFaculty Details:\n";

  f1.getF("FacultyOne", "FacultyCity", 34, 23324, 60000, "Python");
  f1.showF();

  return 0;

}
