#include <iostream>
using namespace std;

// C++ Program to show
// the simply created
// class with public
// access modifier
// and later demonstrates
// the function calling by
// creating an object in the
// main function

class Sum{

public:
  int a,b,c;
  void get()
  {
    cout << "\nEnter 2 values: ";
    cin>>a>>b;
  }
  void add()
  {
    c = a + b;
  }
  void show()
  {
    cout <<"\nSum of "<<a<<" and "<< b <<" is: " <<c;
  }
};

  int main(){

    Sum s1;
    s1.get();
    s1.add();
    s1.show();
    return 0;
}
