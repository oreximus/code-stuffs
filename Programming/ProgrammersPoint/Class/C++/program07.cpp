#include <iostream>
using namespace std;

// Program to Show
// the Working of 
// Heirarchical Inheritance
// in Classes

class Get{
  public:
    int a,b,c;
    void get()
      {
        cout << "\nEnter two values:\n";
        cin >> a >> b;
      }
    void show(string msg){
        cout << "\n"<< msg << " of " << a << " and " << b << " is: " << c;
    }
};

class Sum : public Get{

  public:
  void sum()
    {
      c = a+b;
    }
};

class Prod : public Get{

  public:

  void product()
    {
      c = a*b;
    }

};

int main(){

  Sum s1;
  s1.get();
  s1.sum();
  s1.show("Sum");

  Prod p1;
  p1.get();
  p1.product();
  p1.show("Product");

  return 0;
}
