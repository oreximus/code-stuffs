#include <iostream>
using namespace std;


// Program to show
// the use private function
// and how to access it
// through friend function in
// the main program

class Sum{
  private:
  int a,b,c;
  void get()
  {
    cout << "\nEnter two values: ";
    cin >> a >> b;
  }

  void sum()
  {
    c = a+b;
  }

  void show()
  {
    cout << " Sum of " << a << " and " << b << " is: " << c;
  }

  friend void oper();
};

void oper(){

  Sum s1;
  
  s1.get();
  s1.sum();
  s1.show();
}

int main(){

  oper();

  return 0;
}
