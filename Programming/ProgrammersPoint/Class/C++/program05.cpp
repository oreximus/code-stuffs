#include<iostream>
using namespace std;

// Program Multiple
// Inheritance in
// Classes

class Sum { 
  public:
  int a,b,c;

  void get(){
    cout << "\nEnter the two values: ";
    cin >> a >> b;
  }

  void add(){
    c = a+b;
  }

};

class Product : public Sum{

public:

  void get1(){
    cout << "\nEnter the two values: ";
    cin >> a >> b;
  }
  void pro(){
    c = a*b;
  }

};

class Child : public Sum,public Product {

public:

  void show(){
    cout << "\nSum of " << Sum::a << " and " << Sum::b << " is " << Sum::c;
    cout << "\nProduct of " << Product::a << " and " << Product::b << " is " << Product::c;
  }

};

int main(){

    class Child c1;

    c1.get();
    c1.get1();
    c1.add();
    c1.pro();
    c1.show();

    return 0;
}
