#include <stdio.h>

int main(){

  // Program to factorial of given which is Entered through the keyboard.
  // Factorial, n! = n*(n-1)*(n-2)*...*1

  int i, number;
  long int fact=1;
  printf("\nEnter a Number: ");
  scanf("%d", &number);

  for(i=1; i<=number; i++){
    fact=fact*i;
  }

  printf("Factorial of %d is: %ld\n",number, fact);

  return 0;

}
