#include <stdio.h>
int main(){

  // Program to find
  // a HCF two given
  // number

  int a, b, i, n, hcf;

  printf("\nEnter the Two Values:\n");
  scanf("%d%d",&a,&b);

  if(a<b){
    n = b;
  }
  else{
    n = a;
  }

  for(i=2; i<=n; i++){
    if(a%i == 0 && b%i == 0){
      hcf = i;
    }
    else{
      continue;
    }
  }

  printf("HCF: %d\n", hcf);

  return 0;
}
