#include <stdio.h>

int main(){

  // Program to Check whether the Number Negative, Positive or Zero

  int n,a[10],i;

  printf("\nEnter Numbers:\n");
  for(i=0; i<10; i++){
    scanf("%d", &a[i]);
  }

  printf("\n");

  for(i=0; i<10; i++){
    if(a[i]<0){
      printf("%d is Negative!\n", a[i]);
    }
    else if(a[i]>0){
      printf("%d is Positive!\n", a[i]);
    }
    else{
      printf("%d is Zero\n", a[i]);
    }
  }

  return 0;
}
