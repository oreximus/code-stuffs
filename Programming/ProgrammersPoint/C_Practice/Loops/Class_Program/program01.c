#include <stdio.h>
#include <math.h>

int main(){

  // Program for Series x
  int i,n,c=1,x=1;
  double s=0;

  printf("Enter the Number: ");
  scanf("%d", &n);

  for(i=1; i<=n; i++){

    if(c%2 == 0){

      s = s - pow(x,i);
    }
    else{
      s = s + pow(x,i);
    }
    c ++;
  }
  printf("The Sum of the series: %lf\n", s);

  return 0;
}
