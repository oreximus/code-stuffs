#include <stdio.h>
#include <math.h>

int main(){

  // Program to calculate
  // the naturaul logarithm 
  // of 2, by adding up to
  // n terms in the series
  
  double sum=1,n=1;

  printf("\nEnter the value for n: ");
  scanf("%lf", &n);

  for(double i=2,j=-1;i<n;i++){
    sum += (1.0/i)*j;
    j*=-1;
  }

  printf("Natural Log of 2 = %.4f", sum);

}
