#include <stdio.h>

int main(){

  // Program to Print Hello 5 times
  int i=5;
  printf("\n");

  while(i>0){

    printf("Hello\n");

    i--;
  }
  return 0;
}
