#include <stdio.h>

int main(){

  // Program to print out
  // all Amstrong Numbers Between
  // 1 and 500

  int i,j,n,sum=0;

  for(i=2;i<500;i++){
    n = i;
    while(n != 0){
    if(n<10){
      sum = n*n*n;
    }
    else{
      j = n%10;
      sum = sum + (j*j*j);
      n = n/10;
    }
    }

    if(sum == i){
      printf("%d",i);
    }
  }

  return 0;
}
