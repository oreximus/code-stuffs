#include <stdio.h>

int main(){

  // Program to calculate
  // the sum of the series
  // 1+1/2+1/3....1/n

  int i,n;
  float sum=0;

  printf("\nEnter the value of n: ");
  scanf("%d", &n);

  for(i=1;i<=n;i++){
    sum = sum + (float)1/i;
  }

  printf("The Sum of Series till 1/%d is: %.2f \n", n, sum);

  return 0;
}
