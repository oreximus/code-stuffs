#include <stdio.h>
#include <math.h>

int main(){

  // Program to calculate sum of series
  // x - x^3/3! + x^5/5! - x^7/7! ... x^n/n!
  
  int x,n,i,j=3,f,fact=1;
  double sum;

  printf("\nProgram to Calculate the sin of x for given x,\nusing the series: x - x^3/3! + x^5/5! - x^7/7! ... x^n/n!\n");
  
  printf("\nEnter the value of x: ");
  scanf("%d", &x);
  
  // setting the initial value of the
  // sum to be x, so the first term of
  // the series; x - x^n/n!, can use it!
  // then further in the program the 
  // value of x is reserved and can be 
  // used throughout the series.

  sum = x;

  printf("\nEnter the value of n: ");
  scanf("%d", &n);

  for(i=1; i<=n; i++){
     if(i%2 != 0){
      for(f=2; f<=j; f++){
        fact = fact * f;
      }
      sum = sum - pow(x,j)/fact;
      fact=1;
      j+=2;
     }
     else{
      for(f=2; f<=j; f++){
        fact = fact * f;
      }
      sum = sum + pow(x,j)/fact;
      fact=1;
      j+=2;
     }
  }

  printf("\nThe sum of series is: %lf\n", sum);
  
  return 0;
}
