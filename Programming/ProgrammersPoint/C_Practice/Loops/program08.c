#include <stdio.h>

int main(){

  // Program to check the Number is Prime or Not

  int n,i=2;

  printf("\nEnter the Number: ");
  scanf("%d", &n);

  while(n%i != 0){
    i++;
  }
  if(n != i){
    printf("This is Not a Prime Number!\n");
  }
  else{
    printf("This is a Prime Number!\n");
  }
  return 0;
}
