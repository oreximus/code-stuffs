#include <stdio.h>

int main(){

  // Program to calculate the sum of digits between 1 to 10

  int s=0, i;

  for(i=1; i<=10; i++){

    s = s + i; 
  }

  printf("The Sum of Digits between to 1-10 is: %d\n", s);
  
  return 0;
}
