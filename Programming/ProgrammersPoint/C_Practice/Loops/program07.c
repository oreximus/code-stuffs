#include <stdio.h>

int main(){

  // Program to print the Sum of Digits of Given Integer Number
  
  int n,sum=0;

  printf("\nEnter the Number: ");
  scanf("%d", &n);

  while(n != 0){

    sum = sum+(n%10);
    n = n/10;
  }

  printf("\nSum of all digits are: %d\n", sum);

  return 0;
}
