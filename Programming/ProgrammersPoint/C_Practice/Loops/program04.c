#include <stdio.h>

int main(){

  // Program to print the table with Alternat No
  
  int n, t, c=1, i;

  printf("\nEnter the Number: ");
  scanf("%d", &n);

  for (i=1; i<=20; i++){

  if(i%2 != 0){

    t = n*i;
    printf("%d\n", t);
  }
  else{
    continue;
  }
  
  }
  return 0;
}

