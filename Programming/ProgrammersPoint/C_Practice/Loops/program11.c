#include <stdio.h>

int main(){

  // Program to Print the Reverse Number

  int n,r=0,a;

  printf("\nEnter a Number: ");
  scanf("%d", &n);

  while(n != 0){

    a = n%10;
    r = (r*10) + a;
    n = n/10;
  }
  printf("The Reverse Number is: %d \n", r);
  
  return 0;
}
