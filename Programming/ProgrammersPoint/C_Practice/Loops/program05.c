#include <stdio.h>

int main(){

  // Program to calculate the power
  
  int n,i,p,power;

  printf("\nEnter the Number: ");
  scanf("%d", &n);
  printf("\nEnter the Power: ");
  scanf("%d", &i);

  p = n;

  while(i > 1){

    power = p*n;
    p = power;

    i --;
  }

  printf("\n the power is: %d\n", power);
  return 0;
}
