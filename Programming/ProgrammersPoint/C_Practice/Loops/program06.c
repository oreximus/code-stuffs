#include <stdio.h>

int main(){

  // Program to print all even numbers between 1 to 100
  
  int i;

  for(i=1; i<100; i++){

    if(i%2 == 0){
      printf("%d ", i);
    }
    else{
      continue;
    }
  }
  return 0;
  
}
