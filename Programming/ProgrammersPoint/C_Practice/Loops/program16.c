#include <stdio.h>

int main(){

  // Program to check
  // the Minimum and
  // Maximum value from
  // the user inputs.

  int n,min=0,max=0;

  do{
  printf("\nEnter the Number:");
  scanf("%d", &n);

  if(n>max){
    max = n;
  }
  else if(n<min){
    min = n;
  }
  else{
    continue;
  }
  }
  while(n != 10);

  printf("Minimum Value is: %d\n", min);
  printf("Maximum Value is: %d\n", max);

  return 0;
}
