#include <stdio.h>

int main(){

  // Program to print the table of the Number
  // Input through the Keyboard
  
  int n,i=1,t;

  printf("\nEnter the Number: ");
  scanf("%d", &n);

  while(i <= 10){

    t = n*i;
    printf("%d\n",t);
    i ++;
  }
  return 0;
}
