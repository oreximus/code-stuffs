#include <stdio.h>

int main(){

  // Program to print Numbers from 1 to 10
  //
  int i=1;

  printf("\n");
  do{

    printf("%d\n", i);
    i++;
  }while (i <=10 );
}
