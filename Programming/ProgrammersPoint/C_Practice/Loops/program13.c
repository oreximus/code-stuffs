#include <stdio.h>

int main(){

  // Program to print the Fibonacci Series

  int n,a=0,b=1,i,f;

  printf("\n Enter the Number till Fibonacci Series you want: ");
  scanf("%d", &n);
  printf("Fibonacci Series till %d is: \n",n);

  for(i=0;i<=n;i++){

    f = a + b;
    a = b;
    b = f;

    printf("%d ",f);
  }

  return 0;
}
