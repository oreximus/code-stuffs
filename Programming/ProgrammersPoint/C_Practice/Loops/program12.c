#include <stdio.h>

int main(){

  // Program to check the Entered Number is Palindrome or Not!

  int n, number, a, r=0;

  printf("\n Enter the Number: ");
  scanf("%d", &n);

  number = n;

  while( n != 0){
    a = n%10;
    r = (r*10) + a;
    n = n/10;
  }

  if(r == number){
    printf("This is Palindrome Number!\n");
  }
  else{
    printf("This is Not a Palindrom Number!\n");
  }

  return 0;
}
