#include <stdio.h>
#define size 7

int main(){

  // Program to Cyclically
  // Permute the Elements
  // of an array

  int c=0,n,i,j,a[size];

  printf("\nEnter a number:");
  scanf("%d", &n);

  for(i=0;n/10 == 0;i++){
    a[i] = n%10;
    n = n/10;
    c+=1;
  }

  for(i=0;i<=c;i++){
    printf("%d", a[i]);
  }

  printf("\n%d",c);

  for(i=0;i<c;i++){
    for(j=0;j<=c;j++){
      if(j == c){
        a[j] = a[0];
      }
      else{
        a[j] = a[j+1];
      }
      printf("%d", a[j]);
    }
  }

  return 0;
}
