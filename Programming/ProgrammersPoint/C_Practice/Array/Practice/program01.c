#include <stdio.h>
#define size 5

int main(){

  // Program to check which is a Prime Number
  // Which is not!
  
  int a[size],i,j,c=0;

  printf("Enter %d values: \n", size);

  for(i=0; i<size; i++){
    scanf("%d", &a[i]);
  }
  printf("\nAll Prime No. \n");

  for(i=0; i<size; i++){
    
    for(j=2; j<a[i]; j++){
      if(a[i]%j == 0){
        break;
      }
      if(a[i] == j){
        printf("%d\n", a[i]);
        c++;
      }
    }
  }
  printf("Total Prime Values: %d \n", c);
  return 0;
}
