#include <stdio.h>
#define size 5

int main(){

  // Program to check whether 
  // Entered Number is Prime or Not

  int i,j,a[size],c;

  printf("\nEnter %d Values: ", size);

  for(i=0; i<size; i++){
    scanf("%d", &a[i]);
  }

  printf("All the Prime Values are: \n");

  for(i=0; i<size; i++){
  
    for(j=2; j<a[i]; j++){
        if(a[i]%j == 0){
          break;
        }
        if(a[i] == j){
          printf("%d ", j);
          c++;
        }
      }
  }
  printf("Total Prime Numbers are: %d", c);

  return 0;
}
