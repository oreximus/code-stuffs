#include <stdio.h>
#define size 7

int main(){

  // Program to Sort an Array
  // In Ascending Order

  int a[size],temp,i;

  printf("\nEnter the values: ");

  for(i=0;i<size;i++){
    scanf("%d", &a[i]);
  }

  for(i=0;i<size;i++){
    if(a[i] < a[i+1]){
      temp = a[i];
      a[i] = a[i+1];
      a[i+1] = a[i];
    }
    else{
      continue;
    }
  }
  printf("\nTwo Largest numbers in the array:\n");

  for(i=0;i<2;i++){
    printf("%d\n",a[i]);
  }

  return 0;
}
