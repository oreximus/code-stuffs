#include <stdio.h>
#include <stdlib.h>
#define size 5

int main(){

  // Program to calculate the
  // Sum of the Array Elements
  // using Pointer
  
  int i, sum=0, *a;

  a = (int*)malloc(size * sizeof(int));

  printf("\nEnter the %d values:\n", size);

  for(i=0;i<size;i++){
    scanf("%d", a + i);
    sum += *(a+i);
  }

  printf("\nThe Sum of all %d values are: ", sum);

  return 0;
}
