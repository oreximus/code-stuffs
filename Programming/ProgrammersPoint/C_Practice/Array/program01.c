#include <stdio.h>
#define size 5

int main(){

  // Program Calculate the Sum and Average of Array

  int a[size], i, sum=0;
  float avg;

  printf("\nEnter the %d values: ", size);

  for(i=0; i<size; i++){
    scanf("%d", &a[i]);
  }
  
  for(i=0; i<size; i++){
    sum = sum + a[i];
  }

  avg = (float)sum/size;

  printf("The Sum of all %d values: %d\n", size, sum);
  printf("The Average of all %d values is: %.2f\n", size, avg);

  return 0;
}
