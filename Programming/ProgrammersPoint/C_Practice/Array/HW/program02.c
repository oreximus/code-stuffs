#include <stdio.h>
#define size 5

int main(){

  // Program to print all
  // The elements of Array
  // containg N elements using
  // loop
  
  int i, a[size];

  printf("Enter the Values:\n");

  for(i=0; i<size; i++){
    scanf("%d", &a[i]);
  }

  printf("\nAll values are:");

  for(i=0; i<size; i++){
    printf("%d ", a[i]);
  }

  return 0;
}
