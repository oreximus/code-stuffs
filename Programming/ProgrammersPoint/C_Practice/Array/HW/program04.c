#include <stdio.h>
#define size 5

int main(){

  // Program to calculate the
  // Sum of Even Numbers
  // and their Average
  
  int i, sum=0, a[size], even;
  float avg;

  printf("\nEnter the Values:\n");

  for(i=0; i<size; i++){
    scanf("%d", &a[i]);

    if(a[i]%2 == 0){
      sum = sum + a[i];
      even ++;
    }
    else{
      continue;
    }

  }

    avg = (float)sum/even;

    printf("The Sum of all Even Elements is: %d\n", sum);
    printf("The Average of all Even Elements is: %.2f\n", avg);

  return 0;
}
