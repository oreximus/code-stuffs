#include <stdio.h>

int main(){

  // Sample Program to print the Array Elements
  
  int a[5]={23,45,67,87,21};

  printf("Array Characters:\n%d %d %d %d %d ", a[0],a[1],a[2],a[3],a[4]);

  return 0;
}
