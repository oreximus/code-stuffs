#include <stdio.h>
#define size 5

int main(){

  // Program to Calculate the Sum
  // of All elements
  // and Their Average

  int a[size], i, sum=0;
  float avg;

  printf("\nEnter the Values:\n");

  for(i=0; i<size; i++){
    scanf("%d", &a[i]);
    sum = sum + a[i];
  }

  avg = (float)sum/size;

  printf("The Sum of all Element is : %d\n", sum);

  printf("And the Average is: %.2f\n", avg);

  return 0;

  
}
