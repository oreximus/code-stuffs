#include <stdio.h>
#define size 7

int main(){

  // Program to add a
  // list of even and odd
  // numbers in two separate
  // array
  //
  // Plus the program will
  // get the even and odd 
  // value only when they
  // will appear and get 
  // indexed from 0 to the
  // N numbers of their
  // occurence...

  int a[size], even[size], odd[size], i, e_j=0, o_j=0;

  printf("\nEnter %d values for Even and Odd Numbers:\n", size);
  
  for (i=0;i<size;i++) {
    scanf("%d", &a[i]);

    // Below using e_j
    // and o_j values
    // to keep a track
    // of No of even
    // and odd values

    if(a[i]%2 == 0){
      even[e_j] = a[i];
      e_j ++;
    }
    else{
      odd[o_j] = a[i];
      o_j ++;
    }
  }

  printf("\nEven Values are:\n");

  for(i=0;i<e_j;i++){
    printf("%d\n", even[i]);
  }

  printf("\nOdd Values are:\n");

  for(i=0;i<o_j;i++){
    printf("%d\n", odd[i]);
  }

  return 0;
}
