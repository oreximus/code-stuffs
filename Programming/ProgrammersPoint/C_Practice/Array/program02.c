#include <stdio.h>
#define size 8

int main(){

  // Program to find the
  // largest Two Numbers
  // in a Given array
  
  int i,j,a[size],temp;

  printf("\nEnter the %d values:\n", size);

  for(i=0;i<size;i++){
    scanf("%d", &a[i]);
  }

  for(i=0;i<size-1;i++){
    for(j=0;j<size;j++){
      if(a[j]<a[j+1]){
        temp = a[j];
        a[j] = a[j+1];
        a[j+1] = temp;
      }
    }
  }


  printf("\nTwo Largest Numbers, From all the %d values are: ", size);

  for(i=0;i<2;i++){
    printf("\n%d", a[i]);
  }

  return 0;

}
