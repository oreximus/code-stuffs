#include <stdio.h>
#define size 7

int main(){

  // Program to Insert an
  // Element in a Specific
  // Position in given array

  int i, a[size], p, n;

  printf("\nEnter the Number: ");
  scanf("%d", &n);

  printf("\nEnter the Position for Number: ");
  scanf("%d", &p);

  for(i=0;i<size;i++){
    if(i+1 == p){
      a[i] = n;
    }
    else{
      a[i] = 0;
    }
  }

  for (i=0;i<size;i++) {
    printf("%d position value is: %d\n",i+1,a[i]);
  }

  return 0;
}

