#include <stdio.h>

int main(){

  // Program to check
  // whether the driver is insured
  // or not based on inputs
  // marital status, sex and age

  char status,sex;
  int age;

  printf("\nEnter the Age of driver: ");
  scanf("%d", &age);
  printf("Enter the Marital Status, (y/n):");
  scanf(" %c", &status);
  printf("Gender of the Driver: (m/f):");
  scanf(" %c", &sex);

  if(status == 'y' || status == 'Y'){
    printf("Driver is Insured!\n");
  }
  else if(sex == 'm' || age > 30){
    printf("Driver is Insured! \n");
  }
  else if(sex == 'f' || age > 25){
    printf("Driver is Insured! \n");
  }
  else{
    printf("Driver is Not Insured!\n");
  }

  return 0;

}
