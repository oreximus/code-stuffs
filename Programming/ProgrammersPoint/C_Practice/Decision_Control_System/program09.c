#include <stdio.h>

int main(){

  // Program to Check the 
  // Yougest One by Comparing three
  // Persons Ages Inputs

  int ram, shyam, ajay;

  printf("\nEnter the Age of Ram: ");
  scanf("%d", &ram);

  printf("\nEnter the Age of Shyam: ");
  scanf("%d", &shyam);

  printf("\nEnter the Age of Ajay: ");
  scanf("%d", &ajay);

  if(ram < shyam && ram < ajay){
    printf("Ram is the Yougest!\n");
  }
  else if(shyam < ram && shyam < ajay){
    printf("Shyam is the Yougest!\n");
  }
  else{
    printf("Ajay is the Yougest!\n");
  }

  return 0;

}
