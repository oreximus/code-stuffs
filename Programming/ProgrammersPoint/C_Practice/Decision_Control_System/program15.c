#include <stdio.h>

int main(){

  // Program to find
  // whether a point lies
  // in the x-axis, y-axis
  // or at the origin

  int x,y;

  printf("\nEnter a the value for (x,y): \n");
  scanf("%d%d", &x,&y);

  if(x == 0 && y != 0){
    printf("\nPoint lies on the y-axis!\n");
  }
  else if(x != 0 && y == 0){
    printf("\nPoint lies on the x-axis!\n");
  }
  else if(x == 0 && y == 0){
    printf("\nPoint lies on the Origin!\n");
  }
  else{
    printf("Point is in some Quadrant!\n");
  }

  return 0;
}
