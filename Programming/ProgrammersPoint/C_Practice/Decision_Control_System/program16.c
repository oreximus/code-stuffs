#include <stdio.h>

int main(){

  // Program to check
  // Whether an Entered
  // Character is Symbol,
  // Capital Letter,
  // small case letter or a
  // Number

  char c;

  printf("\nEnter the Character: ");
  scanf("%c", &c);

  if(c>=65 && c<=90){
    printf("\nEntered Character is in Capital Letter!\n");
  }
  else if(c>=97 && c<=122){
    printf("\nEntered Character is in Small Letter!\n");
  }
  else if(c>=48 && c<=57){
    printf("\nEntered Characrer is a Number!\n");
  }
  else{
    printf("\nEntered Character is Some Symbol!\n");
  }

  return 0;
}
