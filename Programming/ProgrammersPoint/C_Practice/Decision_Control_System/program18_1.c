#include <stdio.h>

int main(){

  // Program to check
  // whether entered character
  // is smaller case alphabet or not

  char c;

  printf("\nEnter a Character: ");
  scanf("%c", &c);

  if(c>=97 && c<=122){
    printf("\nIt's a Lower case alphabet!\n");
  }
  else{
    printf("\nIt's Not a Lower Case alphabet!\n");
  }

  return 0;

}
