#include <stdio.h>
#include <math.h>

int main(){

  // Program to check
  // whether the given 
  // coordinates lies outside
  // of the circle, on the circle
  // or inside of the circle
  // By comparing it with given
  // radius.

  int r,x,y;
  float xy_dis;

  printf("\nEnter the Radius: ");
  scanf("%d", &r);
  printf("\nEnter the Coordinates(x,y): ");
  scanf("%d%d", &x,&y);

  xy_dis = pow(x,2) + pow(y,2);

  if(r == sqrt(xy_dis)){
    printf("\nPoint is on the Circle!\n");
  }
  else if(r > sqrt(xy_dis)){
    printf("\nPoint is inside the Circle!\n");
  }
  else if(r < sqrt(xy_dis)){
    printf("\nPoint is outside the Circle!\n");
  }

  return 0;


}
