#include <stdio.h>

int main(){
  // Program to calculate
  // the Area of Rectangle
  // and Checking If it is greater
  // than it's perimeter

  int l,b,area,perimeter;

  printf("\nEnter the Length: ");
  scanf("%d", &l);
  printf("Enter the Breadth: ");
  scanf("%d", &b);

  area = l*b;
  perimeter = 2*(l+b);

  if(area > perimeter){
    printf("The Area of Rectangle is Greater than its Perimeter!\n");
  }
  else{
    printf("The Area of Rectangle is Smaller than its Perimeter!\n");
  }
  return 0;
}
