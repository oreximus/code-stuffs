#include <stdio.h>
int main(){

  // Program to calculate
  // the collinearty of three
  // cordinates

  int x1,y1,x2,y2,x3,y3;

  printf("\nEnter value for x1 and y1: ");
  scanf("%d%d",&x1,&y1);
  printf("\nEnter value for x2 and y2: ");
  scanf("%d%d",&x2,&y2);
  printf("\nEnter value for x3 and y3: ");
  scanf("%d%d",&x3,&y3);

  if((y2-y1)/(x2-x1) == (y3-y2)/(x3-x2)){
    printf("\nThe Points are collinear!\n");
  }
  else{
    printf("\nThey are Not Collinear!\n");
  }
  return 0;
}
