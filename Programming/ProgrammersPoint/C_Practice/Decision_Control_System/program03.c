#include <stdio.h>

int main(){

  // Program to Check the 
  // Percentage of Five Sujects
  // Marks and Printing
  // Pass if Percentage is Greater than 50
  // or Print fail otherwise.

  int a,b,c,d,e,per;

  printf("Enter the Marks for 5 Subjects: \n");
  scanf("%d%d%d%d%d",&a,&b,&c,&d,&e);

  per = (a+b+c+d+e)/5;

  if(per<50){
    printf("You are Failed!\n");
  }
  else{
    printf("Your are Passed!\n");
  }

  return 0;
}
