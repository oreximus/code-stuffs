#include <stdio.h>

int main(){

  // Program to print the Number
  // Only if the Entered Number
  // is Negative

  int n;

  printf("\nEnter a Numnber: ");
  scanf("%d", &n);

  if(n<0){
    printf("Your Output: %d\n", n);
  }

  return 0;
}
