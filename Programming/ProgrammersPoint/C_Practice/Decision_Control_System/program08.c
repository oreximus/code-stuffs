#include <stdio.h>
int main(){

  // Program to Reverse 
  // the Entered Five Digit
  // Number and Check whether they
  // are equal or not!

  int n, r;

  printf("\nEnter a Five Digit Value: ");
  scanf("%d", &n);

  if(n<10000 || n>99999){
    printf("Enter a Valid Input!\n");
  }
  else{
    r = (n%10)*10000 + ((n%100)/10)*1000 + ((n/100)%10)*100 + ((n/1000)%10)*10 + n/10000;

    if(n == r){
      printf("Reverse Number is: %d\n", r);
      printf("And It's Equal to Original Number!\n");
    }
    else{
      printf("Reverse Number is: %d\n", r);
      printf("And It's Not Equal to Original Number!\n");
    }
  }
  return 0;
}
