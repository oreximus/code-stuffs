#include <stdio.h>

int main(){

  // Program to check whether
  // a Triangle is valid or
  // Not by calculating sum of
  // all its angles

  int a,b,c;

  printf("\nEnter the Angle A: ");
  scanf("%d", &a);
  printf("\nEnter the Angle B: ");
  scanf("%d", &b);
  printf("\nEnter the Angle C: ");
  scanf("%d", &c);

  if(a+b+c == 180){
    printf("\nThis is a Valid Triangle!\n");
  }
  else{
    printf("\nIt's Not a Triangle!\n");
  }

  return 0;
}
