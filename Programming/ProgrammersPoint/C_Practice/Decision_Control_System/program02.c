#include <stdio.h>

int main(){

  // Program to check the 
  // Number is Even or Odd

  int n;

  printf("\n Enter a Value: ");
  scanf("%d", &n);

  if(n%2 == 0){
    printf("%d is an Even Number!\n", n);
  }
  else{
    printf("This is an Odd Number!\n");
  }

  return 0;
}
