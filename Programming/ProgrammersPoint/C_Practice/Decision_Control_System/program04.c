#include <stdio.h>

int main(){

  // Program to find the 
  // Entered Year is Leap
  // or Not

  int year;

  printf("\n Enter the Year: ");
  scanf("%d", &year);

  if(year%4 == 0){
    printf("%d is a Leap Year!\n", year);
  }
  else {
    printf("%d is not a Leap Year!\n", year);
  }

  return 0;
}
