#include <stdio.h>

int main(){

  // Program to Find
  // the Absolute Value 
  // of a Number!
  
  int n;

  printf("\nEnter a Number: ");
  scanf("%d", &n);

  if(n<0){
    n = n*-1;
    printf("\nAbsolute Value is: %d\n", n);
  }
  else{
    printf("\nAbsolute Value is: %d\n", n);
  }
  return 0;
}
