#include <stdio.h>

int main(){

  // Program to check
  // Whether an Entered
  // Character is Symbol,
  // Capital Letter,
  // small case letter or a
  // Number

  char c;

  printf("\nEnter the Character: ");
  scanf("%c", &c);

  if(c>=65 && c<=90){
    printf("\nIt's not a Special Symbol Character\n");
  }
  else if(c>=97 && c<=122){
    printf("\nIt's not a Special Symbol Character\n");
  }
  else if(c>=48 && c<=57){
    printf("\nIt's not a Special Symbol Character\n");
  }
  else{
    printf("\nIt's a Special Symbol Character!\n");
  }

  return 0;
}
