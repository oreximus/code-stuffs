#include <stdio.h>

int main(){

  // Program to Check the 
  // Percentage of Five Sujects
  // Marks and Printing
  // Pass if Percentage is Greater than 50
  // or Print fail otherwise.

  int a,b,c,d,e,per;

  printf("Enter the Marks for 5 Subjects: \n");
  scanf("%d%d%d%d%d",&a,&b,&c,&d,&e);

  per = (a+b+c+d+e)/5;

  if(per>=60){
    printf("First Division!\n");
  }
  else if(per>=50 && per<=59){
    printf("Second Division!\n");
  }
  else if(per>=40 && per<=49){
    printf("Third Division!\n");
  }
  else{
    printf("Fail!\n");
  }
  return 0;
}
