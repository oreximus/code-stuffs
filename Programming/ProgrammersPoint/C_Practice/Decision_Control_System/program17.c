#include <stdio.h>

int main(){

  // Program to check
  // the Triangle is Valid
  // or Not Based on the 
  // Entered two Small Sides Sums
  // caparison to the third
  // largest Side!

  int a,b,c,sum,gtr;

  printf("\nEnter the Three Sides Size: ");
  scanf("%d%d%d", &a,&b,&c);

  if(a>b && a>c){
    sum = b+c;
    gtr = a;
  }
  else if(b>a && b>c){
    sum = a+c;
    gtr = b;
  }
  else if(c>a && c>b){
    sum = a+b;
    gtr = c;
  }
  else{
    sum = a+b;
    gtr = a;
  }

  if(sum>gtr){
    printf("\nIt's a Valid Triangle!\n");
  }
  else{
    printf("\nIt's Not a Valid Triangle!\n");
  }

  return 0;

}
