#include <stdio.h>
#include <stdlib.h>

int main(){
  // This pointer will hold the
  // base address of the block created

  int* ptr;

  int n, i;

  // Get the number of element for the array
  printf("\nEnter the number of element: ");
  scanf("%d", &n);
  printf("\nEntered number of elements: %d\n", n);

  // Dynamically allocate memory using malloc()
  ptr = (int*)malloc(n * sizeof(int));

  // Check if the memory has been successfully
  // allocated by alloc or not

  if(ptr == NULL) {
    printf("\nMemory not allocated.\n");
    exit(0);
  }
  else{
    // Memory has been successfully allocated
    printf("\nMemory successfully allocated using malloc.\n");

    for (i=0;i<n;++i){
      ptr[i] = i+1;
    }

    // Print the elements of the array
    printf("\nThe elements of the array are: ");
    for(i=0; i<n; ++i){
      printf("%d, ", ptr[i]);
    }
  }

  return 0;
}
