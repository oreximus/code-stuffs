#include <stdio.h>
#include <ctype.h>

// Program to check
// if the entered charater
// is Alphabet, Digit or
// Special Character
// using library function

void check(){
  char inpt_char;

  printf("\nEnter some data: ");
  scanf("%c", &inpt_char);

  if(isalpha(inpt_char)){
    printf("%c is an Alphabet!\n", inpt_char);
  }
  else if(isdigit(inpt_char)){
    printf("%c is a Number!\n", inpt_char);
  }
  else{
    printf("%c is a Special Charater!\n", inpt_char);
  }
}

int main(){
  check();
  return 0;
}
