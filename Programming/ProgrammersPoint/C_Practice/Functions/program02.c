#include <stdio.h>
#include <math.h>

// Program to calculate
// the area of a triangle

void ta(){

  int a,b,c;
  float area,s;

  printf("\nEnter the First Side Length: ");
  scanf("%d", &a);
  printf("\nEnter the Second Side Length: ");
  scanf("%d", &b);
  printf("\nEnter the Third Side Length: ");
  scanf("%d", &c);

  s = (a+b+c)/2;

  area = sqrt(s*(s-a)*(s-b)*(s-c));

  printf("\nArea of Triangle with %d, %d and %d as Sides, \nis: %.2f", a,b,c,area);
}

int main(){

  ta();

  return 0;

}
