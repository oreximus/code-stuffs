#include <stdio.h>

// Program to create
// a function for Simple
// Interest Calculation

void si(){

  int p,r,t;

  float interest;

  printf("\nEnter two Principle Amount: ");
  scanf("%d", &p);
  printf("\nEnter Rate Percentage: ");
  scanf("%d", &r);
  printf("\nEnter Time Duration you want: ");
  scanf("%d", &t);

  interest = (p*r*t)/100;

  printf("\nSimple Interest for Principle: %d, Rate: %d and Time: %d\n is: %.2f", p,r,t,interest);
}

int main(){

  si();

  return 0;
}
