#include <stdio.h>

int main(){

  // Program to the Interchange their Position.

  int c,d,change;

  printf("\nEnter the Number C: ");
  scanf("%d", &c);
  printf("Enter the Number D: ");
  scanf("%d", &d);

  change = d;
  d = c;
  c = change;
  printf("The Interchanged Value of C: %d and D is: %d\n", c,d);

  return 0;

}
