#include <stdio.h>

int main(){

  //Program to calculate the sum of Middle two digit of 4 digit Number.
  
  int n,last,first,sum,exit,random;

  do{

  printf("\nEnter the 4 digit Number: ");
  scanf("%d", &n);

  
  if(n < 999 || n > 9999){

    printf("Please Enter a Valid 4 digit Number!\n");
    printf("Enter 1 to continue: ");
    scanf("%d", &exit);

  }
  
  else{

    random = n/100;
    first = random%10;
    random = n%100;
    last = random/10;
    sum = first + last;

    printf("The Sum of %d and %d is %d\n", first, last, sum);
    printf("Enter 1 to continue: ");
    scanf("%d", &exit);
  
  }

  } while (exit == 1);  

  return 0;

}
