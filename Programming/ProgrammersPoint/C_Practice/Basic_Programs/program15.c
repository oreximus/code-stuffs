#include <stdio.h>

int main(){

  // Currency notes of denomnation is
  // 10, 50, and 100.
  // Program to Calculate
  // the total Number of Currency
  // notes of each denomnation the casier will
  // have to give to the withdrawler.
  
  int n, ten, fifty, hundred, updt_n;

  printf("\nEnter the Amount: ");
  scanf("%d", &n);

  hundred = n/100;
  updt_n = n%100;
  fifty = updt_n/50;
  updt_n = n%50;
  ten = updt_n/10;
  

  printf("The Total Number of Currency Notes for Each denomnation are:\n");
  printf("Number of 100Rs Notes: %d\n", hundred);
  printf("Number of 50Rs Notes: %d\n", fifty);
  printf("Number of 10Rs Notes: %d\n", ten);

  return 0;
}
