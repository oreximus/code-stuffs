#include <stdio.h>

int main(){

  // Program to calculate the Gross Salary
  // 40 % is the dearness allowance of the basic salary
  // 20 % is the House Rent Allowance
  
  int s;
  float da, ra, gs;

  printf("\nEnter the Basic Salary: ");
  scanf("%d", &s);

  da = s*0.40;
  ra = s*0.20;

  gs = (float)s+da+ra;

  printf("Gross Salary included Dearness Allowance %.2f Rs and House Rent is %.2f Rs : %.2f Rs \n", da, ra, gs);

  return 0;

}
