#include <stdio.h>

int main(){

  // Program to Reverse Four Digit Number
  
  int n, r=0, i, s=0, a, exit;

  do{

  printf("\nEnter a Four Digit Value: \n");
  scanf("%d", &n);

  if(n < 999 || n > 9999){

    printf("Please Enter a Valid 4 digit Number!\n");
    printf("Enter 1 to continue: ");
    scanf("%d", &exit);

  }
  
  else{

    for(i=0; i<4; i++){
      a = n%10;
      r = r*10+(s + a);
      n = n/10;
    }

    printf("The Sum of All Four Digits is: %d\n", r);
    printf("Enter 1 to Continue: ");
    scanf("%d", &exit);
  }

  } while (exit == 1);  

  return 0;
}
