#include <stdio.h>

int main(){

  //Program to Convert the Temperature from Degree Farenheit to Degree Celcius

  int f;
  float c;

  printf("\nEnter the Temperature: ");
  scanf("%d", &f);

  c = ((float)f-32)*5/9;

  printf("Celcius Temperature for %d Farenheit is: %.2f Degree Celcius\n", f,c );

  return 0;
}
