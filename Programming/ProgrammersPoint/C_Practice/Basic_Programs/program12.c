#include <stdio.h>

int main(){

  // Program to calculate the
  // Sum of First and last digit
  // of a Four Digit Number
  
  int n,first,last,sum;

  printf("\nEnter a Four Digit Number: ");
  scanf("%d", &n);

  if(n>9999 || n<1000){
    printf("Enter a Valid Input!\n");
  }
  else{
  first = n/1000;

  last = n%10;

  sum = first + last;

  printf("The Sum of %d and %d is: %d \n", first, last, sum);
  }

  return 0;

}
