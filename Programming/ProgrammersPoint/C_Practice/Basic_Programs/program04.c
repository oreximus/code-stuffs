#include <stdio.h>

int main(){

  // Program calculate the Simple Interest for any Principle Amount
  
  float p,r,t,si;

  printf("\nEnter the Principle Amount: ");
  scanf("%f", &p);

  printf("Enter the Rate Percentage: ");
  scanf("%f", &r);

  printf("Enter the Number of Years: ");
  scanf("%f", &t);

  si = (p*r*t)/100;

  printf("Simple Interest for Principle: %.2f Rs, Rate: %.0f%, Time: %.0f yrs is: %.2f Rs \n", p,r,t,si);

  return 0;
}
