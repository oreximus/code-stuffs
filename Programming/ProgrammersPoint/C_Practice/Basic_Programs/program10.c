#include <stdio.h>

int main(){

  //Program to calculate the sum of first and last digit of 4 digit Number.
  
  int n,last,first,sum,exit;

  do{

  printf("\nEnter the 4 digit Number: ");
  scanf("%d", &n);

  
  if(n < 999 || n > 9999){

    printf("Please Enter a Valid 4 digit Number!\n");
    printf("Enter 1 to continue: ");
    scanf("%d", &exit);

  }
  
  else{
    
    first = n/1000;
    last = n%10;
    sum = first + last;

    printf("The Sum of %d and %d is %d\n", first, last, sum);
    printf("Enter 1 to continue: ");
    scanf("%d", &exit);
  
  }

  } while (exit == 1);  

  return 0;

}
