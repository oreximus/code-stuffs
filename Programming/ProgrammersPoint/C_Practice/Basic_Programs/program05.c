#include <stdio.h>

int main(){

  int h,e,s,m,sst;
  float av;

  printf("\nEnter the English Marks: ");
  scanf("%d", &e);
  printf("Enter the Hindi Marks: ");
  scanf("%d", &h);
  printf("Enter the Science Marks: ");
  scanf("%d", &s);
  printf("Enter the Maths Marks: ");
  scanf("%d", &m);
  printf("Enter the SSt Marks: ");
  scanf("%d", &sst);

  av = (float)(h+e+s+m+sst)/5;

  printf("Average Marks: %.2f\n", av);

  return 0;

}
