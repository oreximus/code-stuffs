#include <stdio.h>

int main(){

  // Program to calculate the Area of Circle and Rectangle
  
  int r, l, b, a;
  float area;

  printf("\nEnter 1 to calculate the Area of Circle or 2 for Area of Rectangle: ");
  scanf("%d", &a);

  if (a == 1){

    printf("Enter the Radius: ");
    scanf("%d", &r);

    area = (float)r*r*3.141;

    printf("The Area of Circle of Radius: %d is: %.2f\n", r, area);
  }
  else if (a == 2) {

    printf("Enter the Length and Breadth: ");
    scanf("%d%d",&l,&b);

    area = l*b;

    printf("The Area of Rectangle of Length %d and Breadth %d is: %d\n", l,b,area);
  }
  else{
    
    printf("Enter the Valid input\n");
  
  }
  
  return 0;
}
