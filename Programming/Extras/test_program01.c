#include <stdio.h>

int main(){

// C Program to Demonstrate returning of day based numberic
// value

  // switch variable
  int var;

  // Taking Input from the user
  
  printf("\nEnter the Input 1/2/3: ");
  scanf("%d",&var);
  // switch statement
  
  switch (var) {
    case 1:
      printf("Case 1 is Matched.\n");
      break;

    case 2:
      printf("Case 2 is Matched.\n");
      break;

    case 3:
      printf("Case 3 is Matched.\n");
      break;

    default:
      printf("Default case is Matched.\n");
      break;
  }

  return 0;

}
