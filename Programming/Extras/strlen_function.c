#include <stdio.h>
#include <string.h>

// Checking the working of strlen function

void main(){

  char str1[] = "Sometext";
  int len;

  printf("Enter a String: ");

  len = strlen(str1);

  printf("String is: %s\n", str1);

  printf("Length of the string is: %d\n", len);
}
