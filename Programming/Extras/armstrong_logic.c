
#include <stdio.h>

int main(){

  // Program to print out
  // all Amstrong Numbers Between
  // 1 and 500

  int i=3234,j,n,sum=0;

    n = i;
    while(n != 0){
      j = n%10;
      sum = sum + (j*j*j);
      n = n/10;
    }

    if(sum == i){
      printf("%d",i);
    }
  

  return 0;
}
