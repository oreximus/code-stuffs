#include <stdio.h>

int main(){
  // Program to print
  // Number Patterns in
  // C

  int n,i,j,sum=0;

  printf("\nEnter the Value of n: ");
  scanf("%d", &n);

  for(i=1;i<=n;i++){
    for(j=1;j<=i;j++){
      sum = sum + 1;
      printf("%d ",sum);
    }
    sum=0;
    printf("\n");
  }

  return 0;
}
