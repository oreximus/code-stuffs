#include <stdio.h>

int main(){

  // Printing numbers
  // pattern in increasing
  // oder by repeating the 
  // number n times where
  // n is the number line.

  int i,j,n;

  printf("\nEnter the Value for N: ");
  scanf("%d",&n);

  for(i=1; i<=n; i++){
    for(j=1; j<=i; j++){
      printf("%d", i);
    }
    printf("\n");
  }

  return 0;
}
