#include <stdio.h>

int main(){

  // Printing Stars 
  // in a Pyramids
  // Manner

  int n,m,i,j,spaces,stars,p,temp,s,c;

  printf("\nEnter the Number of Lines: ");
  scanf("%d", &n);

  if(n%2 == 0){

    c = n*2-1;
    temp = c;
    m = n - 1;
    printf("\n");

    for(i=1;i<=m;i++){

      for(j=1;j<=c;j++){
        spaces = temp/2;
        for(p=0;p<spaces;p++){
          printf(" ");
        }
        
        if(i == 1){
          printf("*");
          for(p=0;p<spaces;p++){
          printf(" ");
        }
          printf("\n");
        }
        else{
          stars = i+2;
          for(s=0;s<stars;s++){
            printf("*");
          }
          for(p=0;p<spaces;p++){
          printf(" ");
        }
          printf("\n");
        }
        temp = spaces;
      }
    }
  }
  else{
    c = n*2-1;
    temp = c;
    n = n - 1;
  
    for(i=1;i<=m;i++){

      for(j=1;j<=c;j++){
        spaces = temp/2;
        for(p=0;p<spaces;p++){
          printf(" ");
        }
        
        if(i == 1){
          printf("*");
          for(p=0;p<spaces;p++){
          printf(" ");
        }
          printf("\n");
        }
        else{
          stars = i+2;
          for(s=0;s<stars;s++){
            printf("*");
          }
          for(p=0;p<spaces;p++){
          printf(" ");
        }
          printf("\n");
        }
        temp = spaces;
      }
    }
  }

    return 0;
}
