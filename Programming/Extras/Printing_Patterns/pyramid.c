#include <stdio.h>

int main(){

  // Program to print
  // the stars in Pyramid
  // pattern for N number
  // of lines entered by the
  // user

  int n,i,j,spaces,stars,temp,p,s;

  printf("\nEnter the Numbers of lines: ");
  scanf("%d",&n);

  temp = n/2;

  if(n%2 == 0){
    printf("\n");
    for(i=1;i<=n;i++){
      spaces = temp;
      stars = i+2;
        for(s=0;s<spaces;s++){
          printf(" ");
        }
        if(i == 1){
          printf("*");
        for(s=0;s<spaces;s++){
          printf(" ");
        }
        printf("\n");
        temp = n-2;
        }
        else {
          for(p=0;p<stars;p++){
            printf("*");
          }
        for(s=0;s<spaces;s++){
          printf(" ");
        }
        printf("\n");
        temp = spaces-2;
        }
    }
  }
  else{
    for(i=1;i<=n;i++){
      spaces = temp;
      stars = i+2;
        for(s=0;s<spaces;s++){
          printf(" ");
        }
        if(i == 1){
          printf("*");
        for(s=0;s<spaces;s++){
          printf(" ");
        }
        printf("\n");
        temp = n-2;
        }
        else {
          for(p=0;p<stars;p++){
            printf("*");
          }
        for(s=0;s<spaces;s++){
          printf(" ");
        }
        printf("\n");
        temp = spaces-2;
        }
    }
  }

  return 0;
}
