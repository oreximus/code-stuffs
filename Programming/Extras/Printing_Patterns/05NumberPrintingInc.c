#include <stdio.h>

int main(){

  // Printing numbers
  // pattern in increasing
  // oder by repeating the 
  // number n times where
  // n is the number line.

  int i,j,n,sum=0;

  printf("\nEnter the Value for N: ");
  scanf("%d",&n);

  for(i=1; i<=n; i++){
    for(j=1; j<=i; j++){
      sum = sum+1;
      printf("%d ", sum);
    }
    printf("\n");
  }

  return 0;
}
