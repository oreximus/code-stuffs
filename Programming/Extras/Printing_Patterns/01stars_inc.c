#include <stdio.h>

int main(){

  // Printing Stars
  // in increasing order
  // for the n number of lines

  int n, i, j;

  printf("\nEnter the Number of Lines: ");
  scanf("%d", &n);

  for(i=n; i>=1; i--){
    for(j=i; j>=1; j--){
      printf("*");
    }
    printf("\n");
  }

  return 0;
}
