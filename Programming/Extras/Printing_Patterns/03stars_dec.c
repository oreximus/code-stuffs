#include <stdio.h>

int main(){

  // printing stars
  // pattern in
  // decreasing order

  int n,i,j;

  printf("\nEnter the Value of N: ");
  scanf("%d", &n);

  for(i=1; i<=n; i++){
    for(j=1; j<=n-i; j++){
      printf(" ");
    }
    for(j=1;j<=i;j++){
      printf("*");
    }
    printf("\n");
  }
  return 0;
}
