#include <stdio.h>
#include <math.h>

int main(){

  // Program to print the
  // Squareroots of Input
  // Number!


  int n;
  float sqrtroot;

  printf("\nEnter a Number: \n");
  scanf("%d", &n);

  sqrtroot = sqrt(n);

  printf("Sqaure of %d is: %.2f \n", n, sqrtroot);

  return 0;
}
