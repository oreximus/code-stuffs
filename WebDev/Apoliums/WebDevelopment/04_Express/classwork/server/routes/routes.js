let express = require("express");
let router = express.Router();
let { square, random, register } = require("../controllers/controller");
var bodyParser = require("body-parser");

// create application/json parser
var jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

router.get("/", function (req, res, next) {
  console.log("Router Working");
  res.send("From Backend!");
  res.end();
});

router.get("/square/:num", square);

router.get("/random", random);

router.post("/register", jsonParser, register);

module.exports = router;
