module.exports = (req, res) => {
    let num = parseInt(req.params.num);
    let square = num*num;
    res.send(`Your square is: ${square}`);
}