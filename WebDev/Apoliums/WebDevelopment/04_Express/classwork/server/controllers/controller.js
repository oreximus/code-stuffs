const bcrypt = require("bcryptjs");

const random = async (req, res) => {
  let random = Math.floor(Math.random() * 100);
  res.send(`Random Number: ${random}`);
};

const square = async (req, res) => {
  let num = parseInt(req.params.num);
  let square = num * num;
  res.send(`Your square is: ${square}`);
};

const register = async (req, res) => {
  let username = req.body.username;
  let password = req.body.password;

  if (/\s/.test(username) || /\s/.test(password)) {
    res.status(400).send(`fields cannot contain any space character!`);
  } else {
    if (username == "" || password == "") {
      res.status(400).send(`all of the fields are required!`);
    } else {
      res.status(200).send(`data sent successfully!`);
      hashedPassword = await bcrypt.hash(password, 8);
    }
  }

  let isMatch = await bcrypt.compare(password, hashedPassword);
  console.log("Passord Match:", isMatch);
  console.log(username);
  console.log(password);
  console.log(hashedPassword);
};

module.exports = {
  square,
  random,
  register,
};
