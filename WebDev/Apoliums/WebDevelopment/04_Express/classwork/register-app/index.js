const express = require("express");

var bodyParser = require("body-parser");

// create application/json parser
var jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

app = express();

app.get("/", (req, res) => {
  res.send("From Backend!");
});

app.post("/register", jsonParser, (req, res) => {
  console.log(req.body);
  res.send("message sent successfully!");
});

app.listen(4000, () => {
  console.log("Server Started!");
});
