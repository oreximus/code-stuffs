# Notes of Backend Learning with ExpressJS:

## How to Create a Express/Node + React Project | Node Backend + React Frontend

**source**: https://www.youtube.com/watch?v=w3vs4a03y3I

- Two folders:

  - client: contain our react code
  - server: contain our express code

- Setup our files in the project

  - cd into the `server` dir
  - run `npm init -y`, to generate `package.json` file.
  - and rename the main 'server.js'
  - and create server.js file in the server directory.
  - install express `npm i express`
  - npm i nodemon -D
  - in package.json file:
    - start: "node server",
    - dev: "nodemon server"

- creating react project in the `client` directory: `npx create-react-app .`
- go to client `src` directory and remove all the boilerplate code from App.js

- go `server.js` file and setup the backend:

```
const express = require('express')
const app = express()

app.get("/api", (req, res) => {
    res.json({ "users": ["userOne", "userTwo", "userThree"] })
})

app.listen(5000, () => {console.log("Server started on port 5000") })
```

- go to server directory and run `npm run dev`.

- and on the browser we can check api data.

- go package.json file:

```
"proxy": "http://localhost:5000",
```

- on the client side `App.js` file:

```
import React, { useEffect, useState } from 'react';

function App() {
    const [backendData, setBackendData] = useState([{}])

    useEffect (() => {
        fetch("/api").then(
            response => response.json()
        ).then(
            data => {
        setBackendData(data)
            }
        )
    }, [])
    return (
        <div>
            {(typeof backendData.users === 'undefined') ? (
                <p>Loading...</p>
            ): (
                backendData.users.map((user, i) => (
                    <p>{user}</p>
                ))
            )}
        </div>
    )
}
```

- go to client and `npm start`

## How to Connect React JS With MySQL Database using Node.JS/Express.js

**source**: https://www.youtube.com/watch?v=Q3ixb1w-QaY

- create a `Backend` directory and cd to it.

```
npm init
```

- then install `express`, `mysql`, `cors`, `nodemon`:

```
npm i express mysql cors nodemon
```

- create `server.js`, under script area: "start": "nodemon server.js"

- then create react app in the directory before `Backend` and create frontEnd directory.

- then start mysql and phpmyadmin.

- in `server.js`:

```
const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

const app = express()
app.use(cors())

app.get('/', (re, res) => {
    return res.json("From Backend Side");
})

app.listen(8081, ()=> {
    console.log("listening");
})
```

- cd to `Backend` directory and `npm start`, if we are getting the message then it's working fine.

- then again in the server.js file:

```
const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

const app = express()
app.use(cors())

const db = mysql.createConnection({
    host: "localhost",
    user: 'root',
    password: '',
    database: 'crud'
})

app.get('/', (re, res) => {
    return res.json("From Backend Side");
})

app.get('/users', (req, res)=> {
    const sql = "SELECT * FROM users";
    db.query(sql, (err, data)=> {
        if(err) return res.json(err);
        return res.json(data);
    })
})

app.listen(8081, ()=> {
    console.log("listening");
})
```

- In our `frontEnd` `App.js` file:

```
import React, { useEffect, useState } from 'react';

function App() {
    const [data, setData] = useState([])
    useEffect(()=> {
        fetch('http://localhost:8001/users')
        .then(res => res.json())
        .then(data => setData(data))
        .catch(err => console.log(err));
    }, [])
    return (
        <div>
            <table>
                <th>ID</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
            </talble>
            <tbody>
                {data.map((d, i) => (
                <tr key={i}>
                    <td>{d.id}</td>
                    <td>{d.name}</td>
                    <td>{d.phone}</td>
                    <td>{d.email}</td>
                ))}
            </tbody>
        </div>
    )
}

export default App;
```

## Register NodeJS Task:

**Task Description**: configure a `/register` route at the backend side and try to send the register
details from the frontend side in the form of json data. And verify these details:

    - if any of the field is blank prompt: "* that info is Required!"
    - if all of the fiels are blank prompt: "data cannot be blank!"
    - and if everything is fine that it will store the data at the backend side!
