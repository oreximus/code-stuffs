const add = (a, b) => {
  return a + b;
};

const sub = (a, b) => {
  return a - b;
};

// exporting multiple function through the object

// module.exports = {
//   add,
//   sub,
// }

// exporting by custom names

module.exports = {
  addFn: add,
  subFn: sub,
};
