const fs = require("fs");

// for creating files sychronously in nodejs
// fs.writeFileSync("./cool.txt", "This is Cool!");

// for creating files asynchronously in nodejs
// fs.writeFileSync("./test.txt", "Hello World Async", (err) => {});
//
//
// reading a file from the local storage

// const results = fs.readFileSync("./cool.txt", "utf-8");

// fs.readFile("./cool.txt", "utf-8", (err, results) => {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log(results);
//   }
// });

// appending the data into a file
//
// fs.appendFileSync("./cool.txt", "there is more u can see!");
//

// copying into some file.
// fs.cpSync("./cool.txt", "test.txt");

// deleting a file

// fs.unlinkSync("./test.txt");

// console.log(fs.statSync("./cool.txt"));

// also whether it is a file or directory
// console.log("\nis this is a file\n");
// console.log(fs.statSync("./cool.txt").isFile());

// creating a folder

// fs.mkdirSync("my-docs");

// also can create folders in a recursive manner:

fs.mkdirSync("cool-docs/folder_one/folder_two", { recursive: true });
