const http = require("http");
const fs = require("fs");
const url = require("url");

// for creating a log file to get the client date and time
// access info

// const myServer = http.createServer((req, res) => {
//   const log = `${Date.now()}: New req Received!\n`;
//   fs.appendFile("log.txt", log, (err, data) => {
//     res.end("Hello Users, How you doin!");
//   });
// });

// for creating a log file to get the client date and time
// with url info and sending response accordingly

const myServer = http.createServer((req, res) => {
  if (req.url === "/favicon.ico") return res.end();
  const log = `${Date.now()}: ${req.method} ${req.url} New Req Received\n`;
  const myUrl = url.parse(req.url, true);
  console.log(myUrl);
  fs.appendFile("log.txt", log, (err, data) => {
    switch (myUrl.pathname) {
      case "/":
        res.end("Homepage");
        break;
      case "/about":
        const username = myUrl.query.myname;
        res.end(`Hi ${username}`);
        break;
      case "/search":
        const search = myUrl.query.search_query;
        res.end("Here are your results for" + search);
        break;
      case "/signup":
        if (req.method === "GET") res.end("This is a SignUp Page!");
        else if (req.method == "POST") {
          // DB Query
          res.end("Success");
        }
      default:
        res.end("404 Not Found");
        break;
    }
  });
});

myServer.listen(8000, () => console.log("Server Started!"));
