const http = require("http");
const fs = require("fs");

// for creating a log file to get the client date and time
// access info

// const myServer = http.createServer((req, res) => {
//   const log = `${Date.now()}: New req Received!\n`;
//   fs.appendFile("log.txt", log, (err, data) => {
//     res.end("Hello Users, How you doin!");
//   });
// });

// for creating a log file to get the client date and time
// with url info and sending response accordingly

const myServer = http.createServer((req, res) => {
  const log = `${Date.now()}: ${req.url} New Req Received\n`;
  fs.appendFile("log.txt", log, (err, data) => {
    switch (req.url) {
      case "/":
        res.end("Homepage");
        break;
      case "/about":
        res.end("This is Cool!");
        break;
      default:
        res.end("404 Not Found");
        break;
    }
  });
});

myServer.listen(8000, () => console.log("Server Started!"));
