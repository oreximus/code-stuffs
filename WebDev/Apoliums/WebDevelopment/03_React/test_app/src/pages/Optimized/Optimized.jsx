import React from "react";
import { useState, Suspense } from "react";
import "./Optimized.css";
import { Container } from "react-bootstrap";
import Header from "../../components/header/header";

const MyComponent = React.lazy(
  () => import("../../components/MyComponent/MyComponent"),
);

const Optimized = () => {
  const [count, setCount] = useState(0);
  return (
    <>
      <Header />
      <Container>
        <div className="main-div">
          <header className="main-header">
            <Suspense fallback={<p>This is Loading...</p>}>
              <MyComponent state={count} />
            </Suspense>
          </header>
          <button onClick={() => setCount((v) => v + 1)}> Increment </button>
          <div className="main-div">
            <h3>Optimized Shit!</h3>
          </div>
        </div>
      </Container>
    </>
  );
};

export default Optimized;
