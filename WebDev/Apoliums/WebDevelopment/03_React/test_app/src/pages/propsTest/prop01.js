import Header from "../../components/header/header";
import { getImageUrl } from "./utils01.js";
import { Container } from "react-bootstrap";

function Avatar({ person, size }) {
  return (
    <img
      className="avatar"
      src={getImageUrl(person)}
      alt={person.name}
      width={size}
      height={size}
      style={{ margin: "20px" }}
    />
  );
}

export default function Profile() {
  return (
    <>
      <Header />
      <Container>
        <Avatar
          size={100}
          person={{
            name: "Katsuko Saruhashi",
            imageId: "YfeOqp2",
          }}
        />

        <Avatar
          size={80}
          person={{
            name: "Aklilu Lemma",
            imageId: "OKS67lh",
          }}
        />

        <Avatar
          size={50}
          person={{
            name: "Lin Lanying",
            imageId: "1bX5QH6",
          }}
        />
      </Container>
    </>
  );
}
