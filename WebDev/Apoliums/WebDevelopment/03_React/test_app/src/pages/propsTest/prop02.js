import Header from "../../components/header/header";
import { Container } from "react-bootstrap";
import { getImageUrl } from "./utils02.js";

const Scientist = (props) => {
  console.log(props.person.name, "<==p");
  return (
    <>
      <section className="profile">
        <h2>{props.person.name}</h2>
        <img
          className="avatar"
          src={getImageUrl(props.person.imageId)}
          alt="Katsuko Saruhashi"
          width={props.size}
          height={props.size}
        />
        <ul>
          <li>
            <b>Profession: </b>
            {props.person.profession}
          </li>
          <li>
            <b>Awards:</b>
            {props.person.awards}
          </li>
          <li>
            <b>Discovered: </b>
            {props.person.discovered}
          </li>
        </ul>
      </section>
    </>
  );
};

export default function Prop02() {
  return (
    <>
      <Header />
      <Container>
        <h1>Notable Scientists</h1>

        <hr />

        <Scientist
          person={{
            name: "Maria Skłodowska-Curie",
            profession: "physicist and chemist",
            awards:
              "(Nobel Prize in Physics, Nobel Prize in Chemistry, Davy Medal, Matteucci Medal)",
            discovered: "polonium (chemical element)",
            imageId: "szV5sdG",
          }}
          size={70}
        />

        <Scientist
          person={{
            name: "Katsuko Saruhashi",
            profession: "geochemist",
            awards: "(Miyake Prize for geochemistry, Tanaka Prize)",
            discovered: "a method for measuring carbon dioxide in seawater",
            imageId: "YfeOqp2",
          }}
          size={70}
        />
      </Container>
    </>
  );
}
