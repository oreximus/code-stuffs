import { Container } from "react-bootstrap";
import Header from "../../components/header/header";

const Home = () => {
    return (
        <>
        <Header />
        <Container>
            <h1>Welcome to Home</h1>
        </Container>
        </>
    );
}

export default Home;