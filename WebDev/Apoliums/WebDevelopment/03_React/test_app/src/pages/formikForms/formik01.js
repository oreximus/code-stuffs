import { Container, Row, Col } from "react-bootstrap";
import { Formik, Form, Field, useFormik, ErrorMessage } from "formik";
import Header from "../../components/header/header";
import "./formik01.css";
import { useState, useEffect } from "react";
import axios from "axios";

const Formik01 = () => {
  return (
    <>
      <Header />
      <Container>
        <div className="form-div">
          <Formik
          initialValues={{fullname: '', email: ''}}
          validate={values => {
            const errors = {};
            if (!values.email) {
              errors.email = 'Requireed';
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
              errors.email = 'Invalid email address';
            }
            return errors;
          }}
          onSubmit={(values, {setSubmitting}) => {
            setTimeout(() => {
              console.log(values);
            }, 400);
          }}
          >
            {({ isSubmitting }) => (
              <Form>
                <Field className="form-field" type="text" name="fullname" placeholder="Enter fullname..."/><br />
                <ErrorMessage name="fullname" component="div" />
                <Field type="text" className="form-field" name="email" placeholder="Enter email..."/><br />
                <ErrorMessage name="email" component="div" />
                <button className="form-field" type="submit" disabled={isSubmitting}>
                  Submit
                </button>
              </Form>
            )}
          </Formik>
        </div>
      </Container>
    </>
  );
};

export default Formik01;
