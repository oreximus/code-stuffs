import { Container, Row, Col } from "react-bootstrap";
import { Formik, Form, Field, useFormik, ErrorMessage } from "formik";
import Header from "../../components/header/header";
import "./formik01.css";
import { useState, useEffect } from "react";
import axios from "axios";
import { getMouseEventOptions } from "@testing-library/user-event/dist/utils";
import "./formik02.css";

const Formik02 = () => {
  const [movieData, setMovieData] = useState([]);
  const movieTitle = "gravity";

  const getMovieData = () => {
    axios
      .get("https://www.omdbapi.com/?apikey=4e9e8ed7&t=" + movieTitle)
      .then((response) => {
        const myRepo = response.data;
        setMovieData(myRepo);
      });
  };

  const movieValues = {
    title: movieData.Title,
    director: movieData.Director,
    writer: movieData.Writer,
    genre: movieData.Genre,
    rated: movieData.Rated,
  };

  console.log(movieValues);

  useEffect(() => getMovieData(), []);

  return (
    <>
      <Header />
      <Container>
        <h1 style={{ marginTop: "10px" }}>Movie Details...</h1>
        <div className="form-div">
          <Formik
            enableReinitialize={true}
            initialValues={movieValues}
            onSubmit={(values) => {
              setTimeout(() => {
                console.log(values);
              }, 400);
            }}
          >
            {({ isSubmitting }) => (
              <Form>
                <Field
                  className="form-field"
                  disabled
                  type="text"
                  name="title"
                />
                <br />
                <Field
                  className="form-field"
                  disabled
                  type="text"
                  name="director"
                />
                <br />
                <Field
                  className="form-field"
                  disabled
                  type="text"
                  name="writer"
                />
                <br />
                <Field
                  className="form-field"
                  disabled
                  type="text"
                  name="genre"
                />
                <br />
                <Field
                  className="form-field"
                  disabled
                  type="text"
                  name="rated"
                />
                <br />
              </Form>
            )}
          </Formik>
        </div>
      </Container>
    </>
  );
};

export default Formik02;
