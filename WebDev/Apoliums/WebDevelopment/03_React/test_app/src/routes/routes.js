import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "../pages/home/home";
import Profile from "../pages/propsTest/prop01";
import Prop02 from "../pages/propsTest/prop02";
import Formik01 from "../pages/formikForms/formik01";
import Formik02 from "../pages/formikForms/formik02";
import Counter from "../pages/reduxTest/counter/Counter";
import Optimized from "../pages/Optimized/Optimized";

const AddRoutes = () => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/prop01" element={<Profile />} />
          <Route path="/prop02" element={<Prop02 />} />
          <Route path="/formik01" element={<Formik01 />} />
          <Route path="/formik02" element={<Formik02 />} />
          <Route path="/redux01" element={<Counter />} />
          <Route path="/optimized" element={<Optimized />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default AddRoutes;
