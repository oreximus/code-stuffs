import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";

const Header = () => {
  return (
    <>
      <Navbar bg="dark" data-bs-theme="dark">
        <Container>
          <Navbar.Brand href="/">CoolWebsite</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/prop01">prop01</Nav.Link>
            <Nav.Link href="/prop02">prop02</Nav.Link>
            <Nav.Link href="/formik01">formik01</Nav.Link>
            <Nav.Link href="/formik02">formik02</Nav.Link>
            <Nav.Link href="redux01">redux01</Nav.Link>
            <Nav.Link href="/optimized">optimized</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
};

export default Header;
