import React, { useMemo, useEffect, useCallback } from "react";
import largeFile from "../../data/large-file.json";

const getLen = (arr) => {
  console.log("I'm calculating!");
  let count = 0;
  for (let i = 0; i < arr.length; i++) {
    count++;
  }
  return count;
};

const MyComponent = (props) => {
  const totalEntries = useMemo(() => getLen(largeFile), []);

  const handleOnWindowLoad = useCallback(() => {
    console.log("Loaded");
  }, []);

  useEffect(() => {
    console.log(props.state);
    window.addEventListener("load", () => handleOnWindowLoad);

    return () => {
      window.removeEventListener("load", () => handleOnWindowLoad);
    };
  }, [props.state, handleOnWindowLoad]);

  return (
    <>
      <h3>Hello from My Component</h3>
      <div>Total Entries {totalEntries}</div>
      <div>Count {props.state}</div>
    </>
  );
};

export default MyComponent;
