import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "../pages/home/home";
import Movie from "../pages/movie/movie";

const AddRoutes = () => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/movie/:movie_name" element={<Movie />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default AddRoutes;
