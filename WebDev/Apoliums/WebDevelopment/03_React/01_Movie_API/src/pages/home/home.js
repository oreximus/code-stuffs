import Header from "../../components/header/header";
import { Container } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Card from "react-bootstrap/Card";
import { useEffect, useState } from "react";
import axios from "axios";
import "./home.css";
import { useParams } from "react-router-dom";

const Home = () => {
  const [movieTitle, setMovieTitle] = useState("Nightcrawler");
  const [movieData, setMovieData] = useState([]);
  let { movie_name } = useParams();

  const getMovieData = () => {
    axios
      .get("https://www.omdbapi.com/?apikey=4e9e8ed7&t=" + movieTitle)
      .then((response) => {
        console.log(response);
        const myRepo = response.data;
        setMovieData(myRepo);
      });
  };

  useEffect(() => getMovieData(), []);

  return (
    <>
      <Header />
      <Container>
        <h1 className="head1">Movies and movies...</h1>
        <hr />
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
          <Form.Label>
            <b>Enter the Movie Name:</b>
          </Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter the Movie Name"
            onChange={(e) => setMovieTitle(e.target.value)}
          />
          <Button variant="primary" size="sm" onClick={getMovieData}>
            Get Details
          </Button>
        </Form.Group>
        <div id="moviediv">
          <Card
            style={{
              width: "18rem",
              marginTop: "20px",
              marginBottom: "20px",
            }}
          >
            <Card.Img variant="top" src={movieData.Poster} />
            <Card.Body>
              <Card.Title style={{ fontSize: "18px" }}>
                {movieData.Title + " " + "(" + movieData.Year + ")"}
              </Card.Title>
              <Button
                variant="primary"
                href={"/movie/" + movieData.Title}
                size="sm"
              >
                Show More
              </Button>
              <br />
            </Card.Body>
          </Card>
        </div>
      </Container>
    </>
  );
};

export default Home;
