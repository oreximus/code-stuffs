import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import SignUpPage from "../pages/fbsignup/fbsignup";

const AddRoutes = () => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<SignUpPage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default AddRoutes;
