import Header from "../../components/header/header";
import { Container, Row, Col } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Accordion from "react-bootstrap/Accordion";
import { useEffect, useState } from "react";
import axios from "axios";
import "./home.css";

const Home = () => {
  const [userData, setUserData] = useState([]);
  const [userName, setUserName] = useState("oreximus");
  const [repoData, setRepoData] = useState([]);

  const getUserData = () => {
    axios.get("https://api.github.com/users/" + userName).then((response) => {
      const myRepo = response.data;
      setUserData(myRepo);
      setRepoData([]);
    });
  };

  const getRepoData = () => {
    axios
      .get("https://api.github.com/users/" + userName + "/repos")
      .then((response) => {
        const RepoData = response.data;
        console.log(RepoData[0].name);
        setRepoData(RepoData);
      });
  };

  const repoDivData = repoData.map((data, inx) => (
    <>
      <Accordion defaultActiveKey="0">
        <Accordion.Item eventKey={inx}>
          <Accordion.Header>
            <b>Repository</b>: {data.name}
          </Accordion.Header>
          <Accordion.Body>
            <Row>
              <Col>
                <b>Clone URL</b>: <br />
                <b>SSH URL</b>:<br />
                <b>Description</b>:<br />
                <b>Forks Count</b>:<br />
                <b>Visibility</b>:<br />
              </Col>
              <Col xs={8}>
                {data.clone_url}
                <br />
                {data.ssh_url}
                <br />
                {data.description}
                <br />
                {data.forks_count}
                <br />
                {data.visibility}
                <br />
              </Col>
            </Row>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </>
  ));

  useEffect(() => {
    getUserData();
  }, []);

  return (
    <>
      <Header />
      <Container>
        <h1 className="head1">Users and Users...</h1>
        <hr />
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
          <Form.Label>
            <b>Enter the Username:</b>
          </Form.Label>
          <Form.Control
            type="text"
            onChange={(e) => {
              setUserName(e.target.value);
            }}
            placeholder="Username is..."
          />
          <Button variant="primary" size="sm" onClick={getUserData}>
            Show Avatar
          </Button>
        </Form.Group>
        <div className="userdiv">
          <Row>
            <Col>
              <img
                src={userData.avatar_url}
                alt="user-avatar"
                style={{ width: "15rem" }}
              />
            </Col>
            <Col>
              <div className="username">
                <h3>{userData.login}</h3>
                <Button variant="dark" size="sm" onClick={getRepoData}>
                  Get Repositories
                </Button>
              </div>
            </Col>
          </Row>
        </div>
        <div className="repodiv">{repoDivData}</div>
      </Container>
    </>
  );
};

export default Home;
