# REACT Notes

## Why React Web Apps Are Better than Plain Websites

**source**: https://hackernoon.com/react-vs-javascript-why-react-web-apps-are-better-than-plain-websites

- Unlike pure JavaScript, apps in React are not created as one long stretch of a file but as small
  building blocks called Components. These Components are Fundamental to React apps.

- Your entire website is divided into these small components which when put together complete the
  UI of the website.

- One major benefit of developing websites this way is that individual components are harmoniously
  developed without hampering other components.

### Faster Speed

- React uses **Virtual DOM** which is a representation of real DOM kept in the memory and
  synchronized with the real DOM.

- Every time a change happens in a components, React compares the updated Virtual DOM with the
  pre-updated version of Virtual DOM to determine what changes were made to the components. React
  then reflects those changes to the real DOM.

- Unlike real DOM, Virtual DOM identifies the minimal changes that are to be made to the DOM and
  only updates that.

- This makes your website super fast when compared to pure JS websites where the whole DOM is
  re-rendered along with the target node.

### Easy to create complex web apps

- For static websites(mostly), Vanilla JavaScript is not a big issue.

- But for complex web apps which require heavy changes to the DOM and fast loading, It becomes a
  hassle to do everything with JS alone.

- Apart from that, React's component-based architecture helps a lot as you can re-use components
  whenever needed.

### Easier to maintain

- One huge benefits of building components based websites is easy maintainability.

- In old times, you were unsure of what effect changing the margin of an HTML element would do on
  your website(overall).

- In React, each component is its own hero. It works and responds to the viewport on its own, no
  worries updating them.

- You can update any component anytime and track its behaviour on different viewports, and then
  repeat the process with other components.

- When done, bring them together to let the magic happen!

### Great Organization

- When you set up your React project with the help of **create-react-app**, React creates a new
  folder for your project with a specific file structure similar to the image below-

![img01](https://hackernoon.imgix.net/images/FpKROxxtLBRmSsCCb4lxCS7LBVr2-k8237te.png)

- Though all the files and folders from the above file structure are important, the one you'll
  use the most in **src**.

- src folder contain all the react components and assets on which you will work while building
  your application.

- Apart from the **public** folder is another important folder, it comes with a default HTML file
  that contains nothing but a **div** in the body.

- This div is used by React to dynamically inject all its code into the wesbite.

- This file structure is great when working on large websites. Unlike the traditional way where
  you can create HTML, CSS, and JS files and store it here and there, this structure adds uniformity
  to your process of building websites.

### Handles UI and Logic Together

- Traditionally the task of building UI is done by HTML and CSS and JavaScript takes care of the
  logic. But this is not true in the case of React.

- React Components the whole process of building websites by providing us with independent
  components that can handle both UI and logic together.

- Each React component takes care of its structure, and logic(and sometimes style too) by itself,
  gone are the days for maintaining separate files of HTML, and JavaScript.

### Updates without reloading:

- Unlike, traditional websites, you don't have to travel from one page to another.

- React manipulates the DOM to change only those components which are required to be changed and
  with the help of _React Router_ navigates the websites from one path to another without reloading.

- This dynamic update of components provides users a seamless browsing experience similar to a
  native app.

### Reusuable Components.

- When building large websites, you'll find that almost all the time you're repeating certain
  patterns, In React, you can group those patterns in a component and reuse them again and again.

- Imagine you're building a blogging website and need to display cards of blog posts on your page.

- Here, all you need to do is create a components for the card and reuse the same components
  everywhere and provide a different dataset to display information of different blogposts.

### React Vs JavaScript: Which is better?

- Final Question: Who won in the battle of React vs JavaScript?

- For web apps or larger websites, clearly React.

- However, you can choose to go with JavaScript too if your website is small and you don't want to
  over-engineer basic things.

## Component and Props in React

- _Components_ are one of the core concepts of React. They are the foundation upon which you build
  use interface (UI).

### Components: UI building blocks

- On the Web, HTML lets us create rich structured documents with its built-in set of tags like
  `<h1>` and `<li>`:

```
  <article>
  <h1>My First Component</h1>
  <ol>
    <li>Components: UI Building Blocks</li>
    <li>Defining a Component</li>
    <li>Using a Component</li>
  </ol>
</article>
```

- This markup represents this article `<article>`, its heading `<h1>`, and an (abbreviated) table
  of contents as an ordered list `<ol>`. Markup like this, combined with CSS for style, and
  JavaScript for interactively, lies behind every sidebar, avatar, modal, dropdown -- every piece of
  UI you see on the Web.

- React lets you combine your markup, CSS, and JavaScript into custom "components", **reusuable UI
  elements for your app**. The table of contents code you saw above could be turned into a
  `<TableOfContents />` component you could render on every page. Under the hood, it still uses the
  same HTML tags lie `<article>`, `<h1>`, etc.

- Just like with HTML tags, you can compose, order and nest components to design whole pages, For
  example, the documentation page you're reading is made out of React components:

```
<PageLayout>
    <NavigationHeader>
        <SearchBar />
        <Link to="/docs">Docs</Link>
    </NavigationHeader>
    <Sidebar />
    <PageContent>
        <TableOfContents />
        <DocumentationText />
    </PageContent>
</PageLayout>
```

- As your project grows, you will notice that many of your designs can be composed by reusing
  components you already wrote, speeding up your development. Our table of contents above could be
  added to any screen with `<TableOfContents />`! You can even jumpstart your project with the
  thousands of component shared by the React open source community like **ChakraUI** and **MaterialUI**.

### Defining a Component:

- Traditionally when creating web pages, web developer marked up their content and then added
  interaction by sprinkling on some JavaScript. This worked greate when interaction was a
  nice-to-have on the web. Now it is expected for many sites and all apps.

- React puts interactivity first while still using the same technology: **a React component is a
  JavaScript function that you can sprinkle with markup**. Here's what that looks like (you can edit
  the example below):

```
export default function Profile() {
  return (
    <img
      src="https://i.imgur.com/MK3eW3Am.jpg"
      alt="Katherine Johnson"
    />
  )
}
```

### Steps to build the Component.

- **Step 1**: `Export the component`: The `export default` is a **standard JavaScript syntax** (not
  specific to React). It lets you mark the main function in a file so that you can late import it
  from other files. (More on importing in _Importing and Exporting Components_).

- **Step 2**: `Define the function`: With `function Profile() {}` define a JavaScript function with
  name `Profile`.

- **Step 3**: `Add markup`: The component returns an `<img />` tag with `src` and `alt` attributes.
  `<img />` is written like HTML, but it is actually JavaScript under the hood! This syntax is
  called `JSX`, and it lets you embed markup inside JavaScript.

- Return statments can be written all on one line, as in this component:

```
return <img src="https://i.imgur.com/MK3eW3As.jpg" alt="Katherine Johnson" />;
```

- But if your markup isn't all on the same line as the `return` keyword, you must wrap it in a
  pair of parenthesis:

```
return (
  <div>
    <img src="https://i.imgur.com/MK3eW3As.jpg" alt="Katherine Johnson" />
  </div>
);
```

### Using a component

- Now you've defined your `Profile` component, you can nest it inside other components. For
  example, you can export a `Gallery` component that uses multiple `Profile`.

```
function Profile() {
  return (
    <img
      src="https://i.imgur.com/MK3eW3As.jpg"
      alt="Katherine Johnson"
    />
  );
}

export default function Gallery() {
  return (
    <section>
      <h1>Amazing scientists</h1>
      <Profile />
      <Profile />
      <Profile />
    </section>
  );
}
```

### What the Browser sees:

- Notice the difference in casing:

  - `<section>` is lowercase, so React knows we refer to an HTML tag.
  - `<Profile />` starts with a capital `P`, so React knows that we want to use our component
    called `Profile`.

- And `Profile` contains even more HTML: `<img />`. In the end, this is what the browser sees:

```
<section>
  <h1>Amazing scientists</h1>
  <img src="https://i.imgur.com/MK3eW3As.jpg" alt="Katherine Johnson" />
  <img src="https://i.imgur.com/MK3eW3As.jpg" alt="Katherine Johnson" />
  <img src="https://i.imgur.com/MK3eW3As.jpg" alt="Katherine Johnson" />
</section>
```

## Creating and nesting components

**source**: react.dev

- React apps are made out of _components_. A component is a piece of the UI (user interface) that has its own logic and appearance.

- A components can be as small as a button, or as large as an entire page.

- React components are JavaScript functions that return markup:

**example**:

**Decalaring Component**:

```

function MyButton(){
    return (
        <button>I'm a button</button>
    );
}

```

**Nesting Component**:

```
export default function MyApp(){
    return (
        <div>
            <h1>Welcome to my app</h1>
            <MyButton />
        </div>
    );
}
```

- Notice that `<MyButton />` starts with a capital letter. That's how you know it's a React component names must always start with a capital letter, while HTML tags must be lowercase.

- The `export default` keywords specify the main component in the file.

### Writing Markup with JSX

- The markup syntax you've seen above is called _JSX_. It is optional, but most React projects use JSX for its convenience.

- JSX is stricter that HTML. You have to close tags like `<br />`. Your component also can't return mutiple JSX tags. You have to wrap them into a shared parent, like a `<div>...</div>` or an empty `<>...</>` wrapper:

### Adding Styles

- In React, you specify a CSS class with `className`. It works the same way as the HTML `class` attribute:

```
<img className="avatar" />
```

Then you write the CSS rules for it in a separate CSS file:

```
/* In you CSS */
.avatar {
    border-radius: 50%;
}
```

- React does not prescribe how you add CSS files. In the simplest case, you'll add a `<link>` tag to your HTML.

- If you use a build tool or a framework, consult its documentation to learn how to add a CSS file to your project.

### Displaying data

- JSX lets you put markup into JavaScript.

- Curly braces let you "escape back" into JavaScript so that you can embed some variable from your code and display it to the user.

- For example, this will display `user.name`:

```
return (
    <h1>
        {user.name}
    </h1>
);
```

- You can also "escape into JavaScript" from JSX attribute, but you have to use curl braces instead of quotes.

For example, `className="avatar"` passes the `"avatar"` string as the CSS class, but `src={user.imageUrl}` reads the JavaScript `user.imageUrl` variable value, and then passes that value as the `src` attribute:

```
return (
    <img
        className="avatar"
        src={user.imageUrl}
    />
);
```

- You can put more complex expressions inside the JSX curl braces too, for example, **string concatenation**:

**App.js**

```
const user = {
    name: 'Hedy Lamerr',
    imageUrl: 'https://i.imgur.com/yXOvdOSs.jpg',
    imageSize: 90,

};

export default function Profile() {
    return (
        <>
            <h1>{user.name}</h1>
            <img
                className="avatar"
                src={user.imageUrl}
                alt={'Photo of' + user.name}
                style={{
                    width: user.imageSize,
                    height: user.imageSize
                }}
            />
        </>
    );
}
```

- In the above example, `style={{}}` is not a special, but a regular `{}` object inside the `style={ }` JSX curly braces. You can use the `style` attribute when your styles depend on JavaScript.

### State: A Component's Memory:

- Component often need to change what's on the screen as a result of an interaction.

Typing into the form should update the input field, clicking "next" on an image carousel should change which image is displayed, clicking "buy" should put a product in the shopping cart.

- Component need to "remember" things: the current input value, the current image, the shopping cart. In React, this kind of component-specific memory is called _state_.

#### Problem with a Regular Variable!

- Clicking the "Next" button should show the next sculpture by changing the `index` to `1`, then `2`, and so on. However this **won't work**!

**App.js**

```

import { sculptureList } from `./data.js`

export defauld function Gallery() {
    let index = 0;

    function handleClick() {
        index = index + 1;
    }

    let sculpture = scultureList[index];
    return (
        <>
            <button onClick={handleClick}>
                Next
            </button>
            <h2>
                <i>{sculpture.name}</i>
                by {sculpture.artist}
            </h2>
            <h3>
                ({index + 1} of {sculptureList.length})
            </h3>
            <img
                src={sculpture.url}
                alt={sculpture.alt}
            />
            <p>
                {sculpture.description}
            </p>
            </>
    );
}
```

- The `handleClick` event handler is updating a local variable, `index`. But two things prevent that change from being visible.

1. **Local variables don't persist between renders**. When React renders this component a second time, it renders it from scratch - it dosen't consider any changes to the local variables.

2. **Changes to local variables won't trigger renders**. React doesn't realize it needs to render the component again with the new data.

- To update a component with new data, two things need to happen:

  1. **Retain** the data between renders.
  2. **Trigger** React to render the component with new data (re-rendering).

- The `useState` Hook provides those two things:
  1. A **state variable** to retain the data between renders.
  2. A **state setter function** to update the variable and trigger React to render the component again.

#### Adding a state variable

- To add state variable, import `useState` from React at the top of the file:

```
import { useState } from 'react';
```

Then, replace this line:

```
let index = 0;
```

with

```
const [index, setIndex] = useState(0);
```

This is how they work together in `handleClick`:

```
function handleClick(){
    setIndex(index + 1);
}
```

**Notes about Component Memory is Specific to the Example Codes in the `demo_app`**

- In React, `useState`, as well as any other functions starting with "`use`", is called a Hook.

> **Hooks - functions starting with `use` - can only be called at the top level of your components or `your own Hooks`**. You can't call Hooks inside conditions, loops, or other nested fuctions. Hooks are functions, but it's helpful to think of them as unconditional declarations about your component's needs. You "use" React features at the top of your component similar to how you "import" modules at the top of your file.

#### Anatomy of `useState`

- When you call `useState`, you are telling React that you want this component to remember something:

```
const [index, setIndex] = useState(0);
```

- In this case, you want React to remember `index`.

> The convention is to name this pair like `const [something, setSomething]`. You could name it anything you like, but conventions make things easier to understand across projects.

- The only argument to `useState` is the **initial value** of your state variable. In this example, the `index`'s initial value is set to `0` with `useState(0)`.

- Every time your component renders, `useState` gives you an array containing two values:

  1. The **state variable** (`index`) with the value you stored.
  2. The **state setter function** (`setIndex`) which can update the state variable and trigger React to render the component again.

- Here's how that happens in action:

```
const [index, setIndex] = useState(0);
```

1. **Your component renders the first time**. Because you passed `0` to `useState` as the initial value for `index`, it will return `[0, setIndex]`. React remembers `0` is the latest state value.

2. **You update the state**. When a user clicks the button, it calls `setIndex(index + 1)`. `index` is `0`, so it's `setIndex(1)`. This tells React to remember `index` is `1` now and triggers another render.

3. **Your component's second render**. React still sees `useState(0)`, but because React remembers that you set `index` to `1`, it returns `[1, setIndex]` instead.

4. And so on!

### Conditional (ternary) operator

**source**: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_operator

- The **conditional (ternary) operator** is the only JavaScript operator that takes three operands:

  1. a condition followed by a question make (`?`),
  2. then an expression to execute if the condition is **truthy** followed by a colon (`:`),
  3. and finally the expression to execute if the condition is **falsy**.

- This operator is frequently used as an alternative to an `if...else` statement.

**Syntax**:

```
condition ? exprIfTrue : exprIfFalse
```

`condition`:

- An expression whose value is used as a condition

`exprIfTrue`:

- An exprssion which is executed if the `condition` evaluates to a **truthy** value (one which equals or can be converted to `true`).

`exprIfFalse`:

- An expression which is executed if the `condition` is **falsy**, (that is, has a value which can be converted to `false`).

#### Description:

- Besides `false`, possible falsy expressions are: `null`, `NaN`, `0`, the empty string (`""`), and `undefined`. If `condition` is any of these, the result of the conditional expression will be the result of executing the expression `exprIfFalse`.

#### Handling null Values:

```
const greeting = (person) => {
    const name = person ? person.name : "stranger";
    return `Howdy, ${name}`;
};

console.log(greeting({ name: "Alice" })); // "Howdy, Alice"
console.log(greeting(null)); // "Howdy, stranger"
```

#### Conditional chains

- The ternary operator is right-associative, which mean it can be "chained" in the following way, similar to an `if ... else if ... else if ... else` chain:

```
function example(){
    return condition ? value1
        : condition ? value2
        : condition ? value3
        : value4;
}
```

- This is equivalend to the following `if...else` chain

```
function example() {
    if (condition1) {
        return value1;
    } else if (condition2) {
        return value2;
    } else if (condition3) {
        return value3;
    } else {
        return value4;
    }
}
```

## Passing Data Deeply with Context:

**source**: https://react.dev/learn/passing-data-deeply-with-context#the-problem-with-passing-props

- Usually, you will pass information from a parent component to a child component via props. But passing props can become verbose and inconvenient if you have to pass them through many components in the middle, or if many components in your app need the same information.

- **Context** lets the parent component make some information available to any component in the tree below it -- no matter how deep -- without passing it explicitly through props.

### Problem with passing props:

- But passing props can become verbose and inconvenient when you need to pass same prop deeply through the tree, or if many components need the same prop. The nearest common ancestor could be far removed from the components that need data, and **lifting state up** that high can lead to a situation called "prop drilling".

### Context: an alternative to passing props

- Context lets a parent component provide data to the entire tree below it. There are many uses for context. Here is one example. Consider this `Heading` component that accepts a `level` for its size:

**App.js**:

```
import Heading from './Heading.js';
import Section from './Section.js';

export default function Page() {
    return (
    <Section>
        <Heading level={1}>Title</Heading>
        <Heading level={2}>Heading</Heading>
        <Heading level={3}>Sub-heading</Heading>
        <Heading level={4}>Sub-sub-heading</Heading>
        <Heading level={5}>Sub-sub-sub-heading</Heading>
        <Heading level={6}>Sub-sub-sub-sub-heading</Heading>
    </Section>
    );
}
```

**Section.js**:

```
export default function Section({children}) {
    return(
    <section>
        {children}
    </section>
    );
}
```

**Heading.js**:

```
export default function Heading({level, childre}) {
    switch (level) {
        case: 1
            return <h1>{childre}</h1>
        case: 2
            return <h2>{childre}</h2>
        case: 3
            return <h3>{childre}</h3>
        case: 4
            return <h4>{childre}</h4>
        case: 5
            return <h5>{childre}</h5>
        case: 6
            return <h6>{childre}</h6>
        default:
            throw Error('Unknown level: ' + level);
    }
}
```

- If we want multiple headings within the same `Section` to always have the same size:

### Axios in React

**source**: https://www.geeksforgeeks.org/axios-in-react-a-guide-for-beginners/

- In React, backend communication is typically achieved using the HTTP protocol. While many developers are familiar with XML HTTP request interface and Fetch API for making HTTP requests, there's another powerful library called Axios that simplifies the process further.

- Axios, which is a popular library is mainly used to send asynchronous HTTP request to REST endpoints. This library is very useful to perform CRUD operations.

  1. This popular library is used to communicate with the backend. Axios supports the Promise API, native to JS ES6.
  2. Using Axios we make API request in our application. Once the request is made we get the data in Return, and then we use this data in our project.
  3. This library is very popular among developers.

### Tutorial for Axios with ReactJS for a REST API

**source**: https://www.youtube.com/watch?v=12l6lkW6JhE

## Classes in JS

**source**: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes

- Classes are a template for creating objects. They encapsulate data with code to work on that data. Classes in JS are built on `prototype` but also have some syntax and semantics that are unique to classes.

### Using classes

**source**:

1. https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_classes
2. https://developer.mozilla.org/en-US/docs/Glossary/Prototype-based_programming

- JavaScript is a prototype-based language - an object's behaviours are specified by its own properties and its prototype's properties. However, with the addition of `classes`, the creaton of hierarchies of objects and the inheritance of properties and their values are much more in line with other object-oriented languages such as Java.

> **Prototype-based programming** is a style of **object-oriented programming** in which **classes** are not explicitly defined, but rather derived by adding properties and methods to an instance of another class or, less frequently, adding them to an empty object.

- In many other languages, _classes_ or _constructors_, are clearly distinguished from objects, or instances. In JavaScript, classes are mainly an abstraction over the existing prototypical inheritacne mechanism - all patterns are convertible to prototype-based inheritance. Classes themselves are normal JavaScript values as well, and have their own prototype chains.

- In fact, most plain JavaScript functions can be used as constructors - you use the `new` operator with a constructor function to create a new object.

## Formik Forms

- The formik library provides an intuitive solution for building forms in React. Formik has a straightforward API and built-in validation, making collecting and manipulating input data in React application easy.

### What is Formik?

- `Formik` is a popular open-source library for building and processing form in React applications. It provides many utility components and functions that make handling form data in a React application more enjoyable.

- Traditional form-management method in React requires creating universal or single `useState()` hook for each form field, adding an event listener to each field, and triggering a method to update them individually.

```
// Infuriating traditional react form management method

import { useState } from "react";

function InputForm() {
  const [input1, setInput1] = useState("");
  const [input2, setInput2] = useState("");

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    if (name === "input1") {
      setInput1(value);
    } else if (name === "input2") {
      setInput2(value);
    }
  };

  const handleSubmit = (event) => {
   // . . .
  };

  return (
    <form onSubmit={handleSubmit}>
      <input name="input1" value={input1} onChange={handleInputChange} />
      <input name="input2" value={input2} onChange={handleInputChange} />
      <button type="submit">Submit</button>
    </form>
  );
}

export default InputForm;
```

- Formik handles all of these tedios operations for us under the hood. We only need to import its provided components - our form data are readily available.

```
<Formik
    onSubmit={(formData)} => {
        console.log(formData);
    }}
>

{({isSubmitting}) => (
    <Form>
        <Field type="text" name="fullname" placeholder="Enter fullname"} />
        <Field type="email" name="email" placeholder="Enter address" />
        <button type="submit">Submit</button>
    </Form>
)}
</Formik>;
```

#### Using **Yup** for Validation:

- Formik supports synchronous and asynchronous form-level and field-level validation.

- It comes with baked-in support for schema-based form-level validation through Yup.

**Form-level Validation**:

- Two ways to do form-level validation with Formik:

  - `<Formik validata>` and `withFormik({ validate: ... })`
  - `<Formik validationSchema>` and `withFormik({validationSchema: ... })`

`**validate**`

- `<Formik>` and `withFormik()` take a prop/option called `validate` that accepts either synchronous or asynchronous function.

```
// Synchronous validation

const validate = (values, props /* only available when using withFormik */) => {
    const errors = {};

    if(!values.email) {
        errors.email = `Required`;
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9._%+-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address';
    }

    //...

    return errors;
};

// Async Validation

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const validate = (values, props /* only available when using withFormik */) => {
    return sleep(2000).then(() => {
        const error = {};
        if (['admin', 'null', 'god'].include(value.usernmae)) {
            errors.username = 'Nice try';
        }
        // ...
        return errors;
    });
};
```
