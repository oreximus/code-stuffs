var student = {name: "David Rayy", sclass: "VI", rollno: 12}

console.log("Roll Property Before Deleting it!");

console.log(student.rollno);

delete student.rollno;

console.log("Roll Property After Deleting it!");

console.log(student.rollno);
